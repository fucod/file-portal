# Base Workspace

The base workspace is for developing web applications based on vue + tailwind + typescript.

# Structure

The project is a monorepo managed by Nx.

## Applications

- Applications are list in `apps`. An applicaiton should be located in a directory named `xxx` which contains `xxx-app` (The main application) and `xxx-app-e2e` (E2E tests).
- The `<app-name>` is defined in `apps/xxx/xxx-app/project.json`.

## Modules

- Modules are listed in `modules`. A modules should be located in a folder named `modules/bw-xxx`.
- The `tsconfig.base.json` in the base-workspace root directory defines all modules in its paths.
- The `<module-name>` is defined in `modules/bw-xxx/project.json`.

# Development

## Prerequisites

- Nix

## Prepare development environment

1. Run `nix-shell` to setup node environment.
2. In nix-shell, run `npm install` to install npm package dependency.

## Commands for Apps

### Run the application dev server

`npx nx serve <app-name>`

### Build application

`npx nx build <app-name>`

### Run test

`npx nx test <app-name>`

## Commands for Modules

### Generate doc

`npx nx gendoc <modules-name>`

### Run test

`npx nx test <modules-name>`

## Create a new application

1. Run `nx g @nx/vue:application xxx-app --directory=apps/xxx/xxx-app` to create a new App.
2. Run `nx g @nx/vue:setup-tailwind --project=xxx-app` to setup tailwind css. To apply common tailwind settings, append the common tailwind config in `apps/xxx/xxx-app/tailwind.config.js` as below.

apps/xxx/xxx-app/tailwind.config.js

```js
const { createGlobPatternsForDependencies } = require('@nx/vue/tailwind');
const { join } = require('path');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [join(__dirname, 'index.html'), join(__dirname, 'src/**/*!(*.stories|*.spec).{vue,ts,tsx,js,jsx}'), ...createGlobPatternsForDependencies(__dirname)],
  theme: {
    extend: {},
  },
  plugins: [],
  // Here
  ...require('../../../modules/bw-core/tailwind.config.js'),
};
```

3. Provide css and images assets in `apps/xxx/xxx-app/src/assets/` and modify `apps/xxx/xxx-app/src/main.ts` to import css index.

```
apps/xxx/xxx-app/src/assets/
├── css
│   ├── app.css
│   ├── core.css
│   ├── index.css
│   └── theme.css
└── images
    └── app
        └── logo-main.svg
```

apps/xxx/xxx-app/src/assets/css/index.css

```css
@import './core.css';
@import './theme.css';
@import './app.css';

@tailwind base;
@tailwind components;
@tailwind utilities;
```

apps/xxx/xxx-app/src/assets/css/core.css

```css
@layer components {
  html {
    @apply subpixel-antialiased;
    cursor: default;
  }

  body {
    height: 100vh;
    height: calc(var(--vh) * 100);
  }

  #root {
    position: relative;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
  }

  div {
    @apply select-none;
    cursor: inherit;
  }

  .select:focus,
  .input:focus,
  textarea:focus {
    outline: 2px solid theme(colors.transparent);
  }

  :focus-visible {
    outline: 2px solid theme(colors.primary);
    outline-offset: 0;
  }

  .menu .bordered button {
    border-left-width: 0.25rem;
  }

  .fade-out-leave-active {
    transition: opacity 0.5s ease;
  }
  .fade-out-leave-to {
    opacity: 0;
  }

  .radio,
  .checkbox {
    border-color: theme(colors.base-300);
  }

  .square-v30 {
    --size: min(30vh, 30vh);
    width: var(--size);
    height: var(--size);
  }
}
```

apps/xxx/xxx-app/src/assets/css/theme.css

```css
@layer components {
  .bg-banner {
    background-repeat: no-repeat;
    background-position: 0% 100%;
  }
  .bg-jumbo {
    background-repeat: no-repeat;
  }
}
```

apps/xxx/xxx-app/src/assets/css/app.css

```css
/* app-scoped styles*/
```

apps/xxx/xxx-app/src/main.ts

```ts
import { createApp } from 'vue';
import { createWebHashHistory, createRouter } from 'vue-router';
import { createPinia } from 'pinia';
import { Startup } from '@base-workspace/bw-core';
import { generateI18n } from '@base-workspace/bw-locale';
import { LOCALE_MAP } from './locales/index';
import { routes } from './services/routes';
import buildMetadata from './build/metadata.json';
import App from './components/app/App.vue';

// Here
import './assets/css/index.css';

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to) => {
  if (to.name !== 'entry' && !Startup.checkReady()) {
    return { name: 'entry', query: to.query };
  }
});

const i18n = generateI18n(LOCALE_MAP, buildMetadata.locale);

const pinia = createPinia();

const app = createApp(App);
app.use(i18n);
app.use(router);
app.use(pinia);
app.mount('#root');
```

4. Provide assets in `apps/xxx/xxx-app/public`.

```
apps/xxx/xxx-app/public
├── favicon.ico
├── logo-main.svg
├── logo-mask-icon.svg
├── logo.svg
└── platform
    └── pwa
        ├── apple-touch-icon-180x180.png
        ├── maskable-icon-512x512.png
        ├── pwa-1024x1024.png
        ├── pwa-128x128.png
        ├── pwa-16x16.png
        ├── pwa-192x192.png
        ├── pwa-256x256.png
        ├── pwa-32x32.png
        ├── pwa-48x48.png
        ├── pwa-512x512.png
        └── pwa-64x64.png
```

5. Modify `apps/xxx/xxx-app/vite.config.ts` to support PWA and bundle size analysis

```ts
/// <reference types='vitest' />
import { PluginOption, defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { nxViteTsPaths } from '@nx/vite/plugins/nx-tsconfig-paths.plugin';
import { VitePWA } from 'vite-plugin-pwa';
import * as appStatic from './src/static/app.json';
import { visualizer } from 'rollup-plugin-visualizer';

export default defineConfig({
  root: __dirname,
  cacheDir: '../../../node_modules/.vite/apps/xxx/xxx-app',
  publicDir: './public',

  server: {
    port: 4200,
    host: 'localhost',
  },

  preview: {
    port: 4300,
    host: 'localhost',
  },

  plugins: [
    vue(),
    nxViteTsPaths(),
    visualizer() as PluginOption,
    VitePWA({
      includeAssets: ['favicon.ico', 'logo-main.svg', 'platform/pwa/logo-mask-icon.svg', 'assets/images/*.svg'],
      manifest: {
        name: appStatic.name,
        short_name: appStatic.shortName,
        description: appStatic.description,
        theme_color: appStatic.mainColor,
        icons: [
          {
            src: 'platform/pwa/pwa-16x16.png',
            sizes: '16x16',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/pwa-32x32.png',
            sizes: '32x32',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/pwa-48x48.png',
            sizes: '48x48',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/pwa-64x64.png',
            sizes: '64x64',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/pwa-128x128.png',
            sizes: '128x128',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/pwa-192x192.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/pwa-256x256.png',
            sizes: '256x256',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/pwa-512x512.png',
            sizes: '512x512',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/pwa-1024x1024.png',
            sizes: '1024x1024',
            type: 'image/png',
          },
          {
            src: 'platform/pwa/maskable-icon-512x512.png',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'maskable',
          },
        ],
      },
      registerType: 'autoUpdate',
      devOptions: {
        enabled: true,
      },
    }),
  ],

  // Uncomment this if you are using workers.
  // worker: {
  //  plugins: [ nxViteTsPaths() ],
  // },

  build: {
    outDir: '../../../dist/apps/xxx/xxx-app',
    reportCompressedSize: true,
    commonjsOptions: {
      transformMixedEsModules: true,
    },
  },

  test: {
    globals: true,
    cache: {
      dir: '../../../node_modules/.vitest/apps/xxx/xxx-app',
    },
    environment: 'jsdom',
    include: ['src/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],

    reporters: ['default'],
    coverage: {
      reportsDirectory: '../../../coverage/apps/xxx/xxx-app',
      provider: 'v8',
    },
  },
});
```

## Create a new module

1. Use Nx commands to generate a new module.

- If the module contains vue components, Use

`npx nx g @nx/vue:lib bw-xxx --directory=modules/bw-xxx`

- If the module a pure js library, Use

`npx nx g @nx/js:lib bw-xxx --directory=modules/bw-xxx`

2. To support doc generation, create a ts config in `modules/bw-xxx/tsconfig.doc.json` and add a `gendoc` target in `modules/bw-xxx/project.json`

modules/bw-xxx/tsconfig.doc.json

```json
{
  "compilerOptions": {
    "lib": ["es2020", "dom"]
  },
  "include": ["src/**/*.ts"],
  "typedocOptions": {
    "name": "bw-xxx",
    "entryPoints": ["./src/index.ts"],
    "out": "docs"
  }
}
```

modules/bw-xxx/project.json

```json
{
  "name": "bw-xxx",
  "$schema": "../../node_modules/nx/schemas/project-schema.json",
  "sourceRoot": "modules/bw-xxx/src",
  "projectType": "library",
  "tags": [],
  "// targets": "to see all targets run: nx show project bw-xxx--web",
  "targets": {
    "gendoc": {
      "command": "typedoc --tsconfig {projectRoot}/tsconfig.doc.json"
    }
  }
}
```
