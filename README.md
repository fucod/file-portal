# File Portal

[![gitlab repo](https://img.shields.io/badge/gitlab-focud/file--portal-blue)](https://gitlab.com/fucod/file-portal)
[![app website](https://img.shields.io/badge/fucod.com-file--portal-blue)](https://file-portal.fucod.com)

![logo](doc/logo-128x128.png)

Magic for freely transferring files between different devices.

# About

This application is a free and open-source project. Any opinions and feedback are welcome.

- App: https://file-portal.fucod.com
- Git Repo: https://gitlab.com/fucod/file-portal

# Development

## Prerequisites

- Nix

## Prepare development environment

1. Run `nix-shell` to setup node environment.
2. In nix-shell, run `npm install` to install npm package dependency.

## Run the application dev server

    npx nx serve potl-app

## Technical Details

- This project is a web application based on `Vue` and `Tailwind Css`, written in `TypeScript`.
- Use `Nx` to manage the project, separating the code specific to the App and the shared modules.
- Using `nix-shell` to construct development environments and deployment processes.

# Support

Support the continuous development of the project with concrete actions. Any form of help means a lot to us.

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/kaitwang)

# Copyright

Copyright (c) 2024 Future Code. Released under the GPLv3 license.
