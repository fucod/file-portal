import app from './app/en-US.json' assert { type: 'json' };
import pages from './pages/en-US.json' assert { type: 'json' };
import components from './components/en-US.json' assert { type: 'json' };
import models from './models/en-US.json' assert { type: 'json' };

export default {
  app,
  pages,
  components,
  models
};
