import app from './app/ja-JP.json' assert { type: 'json' };
import pages from './pages/ja-JP.json' assert { type: 'json' };
import components from './components/ja-JP.json' assert { type: 'json' };
import models from './models/ja-JP.json' assert { type: 'json' };

export default {
  app,
  pages,
  components,
  models
};
