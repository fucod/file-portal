import app from './app/zh-CN.json' assert { type: 'json' };
import pages from './pages/zh-CN.json' assert { type: 'json' };
import components from './components/zh-CN.json' assert { type: 'json' };
import models from './models/zh-CN.json' assert { type: 'json' };

export default {
  app,
  pages,
  components,
  models
};
