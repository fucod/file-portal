import app from './app/zh-TW.json' assert { type: 'json' };
import pages from './pages/zh-TW.json' assert { type: 'json' };
import components from './components/zh-TW.json' assert { type: 'json' };
import models from './models/zh-TW.json' assert { type: 'json' };

export default {
  app,
  pages,
  components,
  models
};
