import { ZodSchema, z } from 'zod';
import type {
  UidAttrs,
  UidParams,
  SortOpt,
  SortData
} from '@base-workspace/bw-model';
import { UidSchema } from '@base-workspace/bw-model';
import { getIcon } from '@base-workspace/bw-icon';

export type TransferUid = string;

export const TUID_SIZE = 32;

const ITEM_TYPES = ['sender', 'receiver'] as const;
export type ItemType = (typeof ITEM_TYPES)[number];

export const ITEM_STATUSES = [
  'created',
  'queued',
  'processing',
  'done',
  'cancelled',
  'failed'
] as const;
export type ItemStatus = (typeof ITEM_STATUSES)[number];

export const ITEM_FORMATS = [
  'text',
  'link',
  'image',
  'file',
  // 'drawing',
  'audio',
  'video'
] as const;
export type ItemFormat = (typeof ITEM_FORMATS)[number];

export type ItemFileInfo = {
  tUid: TransferUid;
  format: ItemFormat;
  fileName: string;
  fileSize: number;
  fileMime: string;
  fileString: string | null;
};

export type ItemAttrs = ItemFileInfo & {
  itemType: ItemType;
  status: ItemStatus;
  createdDt: Date;
  file: File | null;
  chunks: ArrayBuffer[];
  errorCode?: string;
};

export type ItemDoc = ItemAttrs & UidParams;

export type ItemDocParams = Partial<ItemDoc>;

export type ItemExistingDoc = ItemAttrs & UidAttrs;

export type ItemFilterOption = {
  keyword?: string;
  statuses?: ItemStatus[];
  formats?: ItemFormat[];
  sort?: SortOpt;
};

export const VALIDATION_MAP: {
  [key in keyof ItemDocParams]?: ZodSchema;
} = {
  uid: UidSchema.optional(),
  itemType: z.enum(ITEM_TYPES),
  status: z.enum(ITEM_STATUSES),
  createdDt: z.date(),
  tUid: z.string().min(TUID_SIZE),
  format: z.enum(ITEM_FORMATS),
  fileName: z.string(),
  fileSize: z.number().int().min(0),
  fileMime: z.string(),
  file: z.instanceof(File).nullable(),
  fileString: z.string().nullable(),
  chunks: z.instanceof(ArrayBuffer).array(),
  errorCode: z.string().optional()
};

export const SORTING_DATA_MAP: Record<string, SortData> = {
  createdDt_asc: {
    icon: getIcon('mdi', 'sort-clock-ascending-outline'),
    sort: {
      field: 'createdDt',
      order: 'asc'
    }
  },
  createdDt_desc: {
    icon: getIcon('mdi', 'sort-clock-descending-outline'),
    sort: {
      field: 'createdDt',
      order: 'desc'
    }
  },
  fileName_asc: {
    icon: getIcon('mdi', 'sort-alphabetical-ascending'),
    sort: {
      field: 'fileName',
      order: 'asc'
    }
  },
  fileName_desc: {
    icon: getIcon('mdi', 'sort-alphabetical-descending'),
    sort: {
      field: 'fileName',
      order: 'desc'
    }
  }
};
