import * as RandomToken from '@sibevin/random-token';
import type { Uid, BaseModel, SaveResult } from '@base-workspace/bw-model';
import {
  handleModelError,
  sortBy,
  fuzzyCompareString,
  validateParams,
  buildSaveError,
  normalizeDoc
} from '@base-workspace/bw-model';
import type { LabelInfo } from '@base-workspace/bw-ui';
import { getIcon } from '@base-workspace/bw-icon';
import { genUid } from '@base-workspace/bw-model';
import type { LocaleActor } from '@base-workspace/bw-locale';
import { potlDb } from '../storages/db';

import type {
  ItemStatus,
  ItemDoc,
  ItemExistingDoc,
  ItemFilterOption,
  ItemType,
  ItemFormat,
  ItemFileInfo,
  TransferUid
} from './item-types';
import { TUID_SIZE, VALIDATION_MAP } from './item-types';

const FILE_NAME_SIZE = 20;

type ItemSaveResult = SaveResult<Item>;
type ItemBufferResult = SaveResult<ArrayBuffer>;

export class Item implements BaseModel<Item, ItemDoc> {
  doc: ItemDoc;

  public constructor(doc: ItemDoc) {
    this.doc = structuredClone(doc);
  }

  get uid(): Uid | undefined {
    return this.doc.uid;
  }

  get isStored(): boolean {
    return this.uid !== undefined;
  }

  get itemType(): ItemType {
    return this.doc.itemType;
  }

  get status(): ItemStatus {
    return this.doc.status;
  }

  get fileName(): string {
    return this.doc.fileName;
  }

  get fileSize(): number {
    return this.doc.fileSize;
  }

  get fileMime(): string {
    return this.doc.fileMime;
  }

  get file(): File | null {
    return this.doc.file;
  }

  get fileString(): string {
    return this.doc.fileString || '';
  }

  get format(): ItemFormat {
    return this.doc.format;
  }

  get createdDt(): Date {
    return this.doc.createdDt;
  }

  get tUid(): TransferUid {
    return this.doc.tUid;
  }

  get isText(): boolean {
    return Item.isText(this.doc.format);
  }

  get fileSizeWithUnit(): string {
    if (this.fileSize < 1024) {
      return `${this.fileSize} Bytes`;
    } else if (this.fileSize < 1024 * 1024) {
      return `${(this.fileSize / 1024).toFixed(2)} KB`;
    } else if (this.fileSize < 1024 * 1024 * 1024) {
      return `${(this.fileSize / (1024 * 1024)).toFixed(2)} MB`;
    } else {
      return `${(this.fileSize / (1024 * 1024 * 1024)).toFixed(2)} GB`;
    }
  }

  get getBlob(): Blob | null {
    if (this.doc.file) {
      return new Blob([this.doc.file], { type: this.doc.fileMime });
    }
    if (this.doc.chunks.length > 0) {
      return new Blob(this.doc.chunks, { type: this.doc.fileMime });
    }
    if (this.doc.fileString) {
      return new Blob([this.doc.fileString], { type: this.doc.fileMime });
    }
    return null;
  }

  get getFileUrl(): string | null {
    const blob = this.getBlob;
    if (blob) {
      return URL.createObjectURL(blob);
    } else {
      return null;
    }
  }

  get downloadFileName(): string {
    if (this.isText) {
      const dt = this.doc.createdDt;
      return `${Item.buildDateTimeFileName(dt)}.txt`;
    } else {
      return this.doc.fileName;
    }
  }

  get fileInfo(): ItemFileInfo {
    return {
      tUid: this.doc.tUid,
      format: this.doc.format,
      fileName: this.doc.fileName,
      fileSize: this.doc.fileSize,
      fileMime: this.doc.fileMime,
      fileString: this.doc.fileString
    };
  }

  static genTUid(): TransferUid {
    return RandomToken.gen({ length: TUID_SIZE });
  }

  static async createItemByFile(file: File): Promise<ItemSaveResult> {
    const format = Item.fetchFormatFromFile(file);
    const doc: ItemDoc = {
      itemType: 'sender' as ItemType,
      status: 'created',
      tUid: Item.genTUid(),
      format,
      fileName: file.name,
      fileSize: file.size,
      fileMime: file.type,
      file,
      fileString: null,
      createdDt: new Date(),
      chunks: []
    };
    const result = validateParams(doc, VALIDATION_MAP);
    if (!result.gain) {
      return buildSaveError(result.reason);
    }
    const item = new Item(doc);
    return await item.save();
  }

  static async createItemByText(
    text: string,
    itemType: ItemType,
    status: ItemStatus
  ): Promise<ItemSaveResult> {
    let format: ItemFormat = 'text';
    try {
      const url = new URL(text);
      if (url.protocol === 'http:' || url.protocol === 'https:') {
        format = 'link';
      }
    } catch (_) {
      format = 'text';
    }
    let fileName = text;
    if (format === 'text' && text.length > FILE_NAME_SIZE) {
      fileName = `${text.substring(0, FILE_NAME_SIZE)} ...`;
    }
    const doc: ItemDoc = {
      itemType,
      status,
      tUid: Item.genTUid(),
      format,
      fileName,
      fileSize: text.length,
      fileMime: 'text/plain',
      file: null,
      fileString: text,
      createdDt: new Date(),
      chunks: []
    };
    const result = validateParams(doc, VALIDATION_MAP);
    if (!result.gain) {
      return buildSaveError(result.reason);
    }
    const item = new Item(doc);
    return await item.save();
  }

  static async createItemByFileInfo(
    fileInfo: ItemFileInfo
  ): Promise<ItemSaveResult> {
    const doc: ItemDoc = Object.assign(fileInfo, {
      itemType: 'receiver' as ItemType,
      status: 'processing' as ItemStatus,
      file: null,
      createdDt: new Date(),
      chunks: []
    });
    if (doc.format === 'text' || doc.format === 'link') {
      doc.status = 'done';
    }
    const result = validateParams(doc, VALIDATION_MAP);
    if (!result.gain) {
      return buildSaveError(result.reason);
    }
    const item = new Item(doc);
    return await item.save();
  }

  static fetchFormatFromFile(file: File) {
    switch (file.type) {
      case 'image/apng':
      case 'image/gif':
      case 'image/jpeg':
      case 'image/png':
      case 'image/svg+xml':
      case 'image/webp':
        return 'image';
      case 'audio/mp4':
      case 'audio/mpeg':
      case 'audio/ogg':
      case 'audio/wave':
        return 'audio';
      case 'video/mp4':
      case 'video/ogg':
      case 'video/webm':
        return 'video';
      default:
        return 'file';
    }
  }

  static async fetch(uid: Uid): Promise<Item | null> {
    const doc = await potlDb.items.get({ uid });
    if (!doc) {
      return null;
    }
    return new Item(doc);
  }

  static async fetchByTUid(
    itemType: ItemType,
    tUid: TransferUid
  ): Promise<Item | null> {
    const doc = await potlDb.items.get({ itemType, tUid });
    if (!doc) {
      return null;
    }
    return new Item(doc);
  }

  static async fetchItems(opts: {
    itemType?: ItemType;
    statuses?: ItemStatus[];
  }): Promise<Item[]> {
    return (
      await potlDb.items
        .where({ itemType: opts.itemType })
        .filter((doc) => {
          if (opts.statuses) {
            return opts.statuses.includes(doc.status);
          } else {
            return true;
          }
        })
        .toArray()
    ).map((doc) => {
      return new Item(doc);
    });
  }

  static filterItems(items: Item[], opts: ItemFilterOption = {}): Item[] {
    let filteredItems = items;
    if (opts.statuses && opts.statuses.length > 0) {
      filteredItems = filteredItems.filter((item) => {
        if (opts.statuses && opts.statuses.length > 0) {
          return opts.statuses.includes(item.status);
        } else {
          return true;
        }
      });
    }
    if (opts.formats && opts.formats.length > 0) {
      filteredItems = filteredItems.filter((item) => {
        if (opts.formats && opts.formats.length > 0) {
          return opts.formats.includes(item.format);
        } else {
          return true;
        }
      });
    }
    if (opts.keyword) {
      filteredItems = filteredItems.filter((item) => {
        return (
          fuzzyCompareString(item.fileName, opts.keyword) ||
          (item.format === 'text' &&
            fuzzyCompareString(item.fileString, opts.keyword))
        );
      });
    }
    filteredItems = sortBy(filteredItems, opts.sort || { field: 'createdDt' });
    return filteredItems;
  }

  static async deleteItems(uids: Uid[]) {
    await potlDb.items.bulkDelete(uids);
  }

  static buildDateTimeFileName(dt: Date): string {
    const paddingWithZero = (val: number) => val.toString().padStart(2, '0');
    const yyyy = dt.getFullYear();
    const MM = paddingWithZero(dt.getMonth() + 1);
    const DD = paddingWithZero(dt.getDate());
    const hh = paddingWithZero(dt.getHours());
    const mm = paddingWithZero(dt.getMinutes());
    const ss = paddingWithZero(dt.getSeconds());
    return `${yyyy}-${MM}-${DD}_${hh}-${mm}-${ss}`;
  }

  public async save(): Promise<ItemSaveResult> {
    let docToSave = Object.assign({}, this.doc);
    let action = 'update';
    if (!this.doc.uid) {
      const idy = await Item.genUniqueIdy();
      docToSave = Object.assign(docToSave, idy);
      action = 'create';
    }
    const result = validateParams(docToSave, VALIDATION_MAP);
    if (!result.gain) {
      return buildSaveError(result.reason);
    }
    let errorCode: string | undefined;
    const normalizedDoc = normalizeDoc(docToSave) as ItemExistingDoc;
    await potlDb.items.put(normalizedDoc).catch(function (error) {
      handleModelError(error, (code: string) => {
        errorCode = code;
      });
    });
    if (errorCode) {
      return buildSaveError(errorCode);
    } else {
      this.doc = docToSave;
      return { target: this, action };
    }
  }

  public assignFileString(text: string) {
    this.doc.fileString = text;
    if (text.length > FILE_NAME_SIZE) {
      this.doc.fileName = `${text.substring(0, FILE_NAME_SIZE)} ...`;
    } else {
      this.doc.fileName = text;
    }
  }

  public async reload(): Promise<boolean> {
    if (this.uid) {
      return false;
    }
    const fetchedDoc = await potlDb.items.get({ uid: this.uid });
    if (fetchedDoc) {
      this.doc = fetchedDoc;
      return true;
    } else {
      return false;
    }
  }

  public async abort(): Promise<ItemSaveResult> {
    if (this.status === 'queued') {
      this.doc.status = 'created';
    }
    if (this.status === 'processing') {
      this.doc.status = 'cancelled';
      this.doc.chunks = [];
      this.doc.tUid = Item.genTUid();
    }
    return this.save();
  }

  public async resetToSend(): Promise<ItemSaveResult> {
    this.doc.status = 'queued';
    this.doc.tUid = Item.genTUid();
    return this.save();
  }

  public async readFileBuffer(): Promise<ItemBufferResult> {
    return new Promise((resolve) => {
      if (!this.doc.file) {
        resolve(buildSaveError('item_without_file'));
        return;
      }
      const fileReader = new FileReader();
      fileReader.onerror = (error) => {
        console.error('Error reading file:', error);
      };
      fileReader.onabort = (event) => {
        console.log('aborted:', event);
      };
      fileReader.onload = async (event) => {
        if (!event.target || !event.target.result) {
          resolve(buildSaveError('no_file_data'));
          return;
        }
        resolve({ target: event.target.result as ArrayBuffer });
      };
      fileReader.readAsArrayBuffer(this.doc.file);
    });
  }

  public putChunk(chunk: ArrayBuffer) {
    this.doc.chunks.push(chunk);
  }

  public async storeError(errorCode: string): Promise<ItemSaveResult> {
    this.doc.status = 'failed';
    this.doc.errorCode = errorCode;
    return await this.save();
  }

  public formatLabelInfo(la: LocaleActor): LabelInfo {
    return Item.formatLabelInfo(this.format, la);
  }

  public statusLabelInfo(la: LocaleActor): LabelInfo {
    return Item.statusLabelInfo(this.itemType, this.status, la);
  }

  public sizeDisplay(la: LocaleActor): string {
    if (this.format === 'text') {
      return la.t(`models.item.field.fileSize.options.wordCount`, {
        count: this.fileSize
      }) as string;
    } else if (this.format === 'link') {
      return la.t(`models.item.field.fileSize.options.charCount`, {
        count: this.fileSize
      }) as string;
    } else {
      return this.fileSizeWithUnit;
    }
  }

  public async switchStatus(status: ItemStatus): Promise<ItemSaveResult> {
    this.doc.status = status;
    return await this.save();
  }

  public async delete(): Promise<ItemSaveResult> {
    let errorCode: string | undefined;
    if (!this.uid) {
      return buildSaveError('not_stored');
    }
    await potlDb.items.delete(this.uid).catch(function (error) {
      handleModelError(error, (code: string) => {
        errorCode = code;
      });
    });
    if (errorCode) {
      return buildSaveError(errorCode);
    } else {
      return { target: this };
    }
  }

  static async genUniqueIdy(): Promise<{
    uid: string;
  }> {
    let uid, foundUid;
    do {
      uid = genUid();
      foundUid = await potlDb.items.where({ uid }).first();
    } while (foundUid);
    return { uid };
  }

  static statusLabelInfo(
    itemType: ItemType,
    status: string,
    la: LocaleActor
  ): LabelInfo {
    const text = la.t(
      `models.item.field.status.options.${itemType}.${status}`
    ) as string;
    switch (status) {
      case 'created':
        return {
          text,
          colorName: 'warning',
          iconInfo: { icon: getIcon('mdi', 'plus') }
        };
      case 'queued':
        return {
          text,
          colorName: 'info',
          iconInfo: { icon: getIcon('mdi', 'dots-circle') }
        };
      case 'processing':
        return {
          text,
          colorName: 'info',
          iconInfo: { icon: getIcon('mdi', 'play') }
        };
      case 'done':
        return {
          text,
          colorName: 'success',
          iconInfo: { icon: getIcon('mdi', 'check') }
        };
      case 'failed':
        return {
          text,
          colorName: 'error',
          iconInfo: { icon: getIcon('mdi', 'close') }
        };
      case 'cancelled':
        return {
          text,
          colorName: 'base-300',
          iconInfo: { icon: getIcon('mdi', 'cancel') }
        };
      default:
        return {
          text,
          iconInfo: { icon: getIcon('mdi', 'circle') }
        };
    }
  }

  static formatLabelInfo(format: string, la: LocaleActor): LabelInfo {
    const text = la.t(`models.item.field.format.options.${format}`) as string;
    switch (format) {
      case 'text':
        return {
          text,
          iconInfo: { icon: getIcon('mdi', 'text-box-outline') }
        };
      case 'link':
        return {
          text,
          iconInfo: { icon: getIcon('mdi', 'link-variant') }
        };
      case 'image':
        return { text, iconInfo: { icon: getIcon('mdi', 'image') } };
      case 'file':
        return {
          text,
          iconInfo: { icon: getIcon('mdi', 'file-outline') }
        };
      case 'drawing':
        return { text, iconInfo: { icon: getIcon('mdi', 'draw') } };
      case 'audio':
        return {
          text,
          iconInfo: { icon: getIcon('mdi', 'volume-source') }
        };
      case 'video':
        return {
          text,
          iconInfo: { icon: getIcon('mdi', 'filmstrip') }
        };
      default:
        return {
          text,
          iconInfo: { icon: getIcon('mdi', 'circle') }
        };
    }
  }

  static isText(format: ItemFormat): boolean {
    return format === 'text' || format === 'link';
  }
}
