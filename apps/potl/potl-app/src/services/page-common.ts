import * as zip from '@zip.js/zip.js';
import type { LocaleActor } from '@base-workspace/bw-locale';
import type { LabelInfo } from '@base-workspace/bw-ui';
import type { ItemType } from '../models/item-types';
import { Item } from '../models/item';

export function laKey(key: string): string {
  return `pages.common.${key}`;
}

export async function buildDownloadZipBlobUrl(items: Item[]): Promise<string> {
  const zipWriter = new zip.ZipWriter(new zip.BlobWriter('application/zip'), {
    bufferedWrite: true
  });
  const fileNames: string[] = [];
  items.forEach((item) => {
    const blob = item.getBlob;
    if (blob) {
      let fileName = item.downloadFileName;
      if (fileNames.includes(fileName)) {
        const fileExt = fileName.split('.').pop();
        const fileNameWithoutExt = fileName.replace(`.${fileExt}`, '');
        fileName = `${fileNameWithoutExt}_${item.uid}.${fileExt}`;
      }
      fileNames.push(fileName);
      zipWriter.add(fileName, new zip.BlobReader(blob));
    }
  });
  return URL.createObjectURL(await zipWriter.close());
}

export function fetchListItemLabelInfo(
  la: LocaleActor,
  itemType: ItemType,
  item: Item,
  inProcessing: boolean
): LabelInfo {
  if (inProcessing) {
    return Item.statusLabelInfo(itemType, 'processing', la);
  } else {
    return item.statusLabelInfo(la);
  }
}
