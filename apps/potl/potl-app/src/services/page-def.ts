import * as PageGuide from '@base-workspace/bw-page-guide';
import { getIcon } from '@base-workspace/bw-icon';

export const PAGES: PageGuide.PageGuideData[] = [
  {
    code: 'entry',
    route: { name: 'entry' },
    nameLk: 'app.service.home',
    iconInfo: {
      icon: getIcon('mdi', 'home')
    }
  },
  {
    code: 'sender',
    route: { name: 'sender_connection' },
    nameLk: 'pages.sender.title',
    iconInfo: {
      icon: getIcon('mdi', 'arrow-top-right')
    }
  },
  {
    code: 'sender-connection',
    parent: 'sender',
    route: { name: 'sender_connection' },
    nameLk: 'pages.sender.connection.title',
    iconInfo: {
      icon: getIcon('mdi', 'connection')
    }
  },
  {
    code: 'sender-list',
    parent: 'sender',
    route: { name: 'sender_list' },
    nameLk: 'pages.sender.list.title',
    iconInfo: {
      icon: getIcon('mdi', 'format-list-bulleted-square')
    }
  },
  {
    code: 'receiver',
    route: { name: 'receiver_connection' },
    nameLk: 'pages.receiver.title',
    iconInfo: {
      icon: getIcon('mdi', 'tray-arrow-down')
    }
  },
  {
    code: 'receiver-connection',
    parent: 'receiver',
    route: { name: 'receiver_connection' },
    nameLk: 'pages.receiver.connection.title',
    iconInfo: {
      icon: getIcon('mdi', 'connection')
    }
  },
  {
    code: 'receiver-list',
    parent: 'receiver',
    route: { name: 'receiver_list' },
    nameLk: 'pages.receiver.list.title',
    iconInfo: {
      icon: getIcon('mdi', 'format-list-bulleted-square')
    }
  },
  {
    code: 'about',
    route: { name: 'about' },
    nameLk: 'pages.about.title',
    iconInfo: {
      icon: getIcon('mdi', 'star')
    }
  },
  {
    code: 'settings',
    route: { name: 'settings' },
    nameLk: 'pages.settings.title',
    iconInfo: {
      icon: getIcon('mdi', 'cog')
    }
  }
];
