const EntryPage = () => import('../pages/Entry.vue');
const SenderConnectionPage = () => import('../pages/SenderConnection.vue');
const SenderListPage = () => import('../pages/SenderList.vue');
const ReceiverConnectionPage = () => import('../pages/ReceiverConnection.vue');
const ReceiverListPage = () => import('../pages/ReceiverList.vue');
const ErrorPage = () => import('../pages/Error.vue');
const AboutPage = () => import('../pages/About.vue');
const SettingsPage = () => import('../pages/Settings.vue');

export const routes = [
  {
    path: '/',
    name: 'entry',
    component: EntryPage
  },
  {
    path: '/sender/connection',
    name: 'sender_connection',
    component: SenderConnectionPage
  },
  {
    path: '/sender/list',
    name: 'sender_list',
    component: SenderListPage
  },
  {
    path: '/receiver/connection',
    name: 'receiver_connection',
    component: ReceiverConnectionPage
  },
  {
    path: '/receiver/list',
    name: 'receiver_list',
    component: ReceiverListPage
  },
  {
    path: '/about',
    name: 'about',
    component: AboutPage
  },
  {
    path: '/settings',
    name: 'settings',
    component: SettingsPage
  },
  {
    path: '/error',
    name: 'error',
    component: ErrorPage
  },
  {
    path: '/:pathMatch(.*)*',
    component: EntryPage
  }
];
