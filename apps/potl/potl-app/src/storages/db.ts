import type { Table } from 'dexie';
import Dexie from 'dexie';
import type { ItemExistingDoc } from '../models/item-types';

export class PotlDb extends Dexie {
  items!: Table<ItemExistingDoc, string>;

  constructor() {
    super('PotlDb');
    this.version(1).stores({
      items: '&uid, &[itemType+tUid], createdDt'
    });
  }
}

export const potlDb = new PotlDb();
