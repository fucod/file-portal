import 'fake-indexeddb/auto';
import { expect } from 'vitest';
import { genUid } from '@base-workspace/bw-model';
import appLocal from './local'; // Replace with your actual module path

describe('appLocal', () => {
  it('writes and reads the lastLoginUserUid', () => {
    const key = 'lastLoginUserUid';
    const value = genUid();
    appLocal.write(key, value);
    expect(appLocal.read(key)).toBe(value);
  });

  it('overwrites existing keys', () => {
    const key = 'lastLoginUserUid';
    const firstValue = genUid();
    const secondValue = UidUtility.genUid();
    appLocal.write(key, firstValue);
    expect(appLocal.read(key)).toBe(firstValue);
    appLocal.write(key, secondValue);
    expect(appLocal.read(key)).toBe(secondValue);
  });
});
