import { useStorage } from '@vueuse/core';

type LocalDataType = {
  lastSenderItemsSorting?: string;
  lastReceiverItemsSorting?: string;
};

const appLocal = useStorage<LocalDataType>('pybk-local', {});

function read(key: keyof LocalDataType): string | undefined {
  return appLocal.value[key];
}

function write(key: keyof LocalDataType, value: string) {
  const newData: LocalDataType = {};
  newData[key] = value;
  appLocal.value = Object.assign({}, newData);
}

export default { write, read };
