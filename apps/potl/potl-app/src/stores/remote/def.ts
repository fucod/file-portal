import { ItemFileInfo, TransferUid } from '../../models/item-types';

export type PeerId = string;
export type ClientId = string;
export type RemoteError = {
  action: string;
  code: string;
  extra?: Record<string, string>;
};

export type RemoteMsgRegister = {
  cid: ClientId;
};

export type RemoteMsgCid = {
  cid: ClientId;
};

export type RemoteMsgDone = {
  action: string;
  tUid?: TransferUid;
  extra?: Record<string, string>;
};

export type RemoteMsgTransfer = {
  cid: ClientId;
  fileInfo: ItemFileInfo;
};

export type TransferInfo = {
  fileInfo: ItemFileInfo;
  progress: number;
};
