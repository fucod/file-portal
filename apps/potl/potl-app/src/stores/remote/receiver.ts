import { ref } from 'vue';
import { defineStore } from 'pinia';
import { joinRoom, selfId } from 'trystero';
import type { Room } from 'trystero';
import * as RandomToken from '@sibevin/random-token';
import appStatic from '../../static/app.json';
import type {
  RemoteError,
  RemoteMsgRegister,
  RemoteMsgCid,
  ClientId,
  PeerId,
  RemoteMsgTransfer
} from './def';
import { Item } from '../../models/item';
import { TransferUid } from '../../models/item-types';

type ReceiverStatus = 'none' | 'ready' | 'connected';
type SenderStatus = 'ready' | 'inAction';

type ClientInfo = {
  cids: ClientId[];
  peerIdMap: Record<ClientId, string>;
  statusMap: Record<ClientId, SenderStatus>;
};

export const useRemoteReceiverStore = defineStore(
  'pybk-remote-receiver-store',
  () => {
    const code = ref<string | null>(null);
    const room = ref<Room | null>(null);
    const isConnected = ref<boolean>(false);
    const error = ref<RemoteError | null>(null);
    const connectedClients = ref<ClientInfo>({
      cids: [],
      peerIdMap: {},
      statusMap: {}
    });
    const progressMap = ref<Record<TransferUid, number>>({});

    async function openRemote() {
      if (room.value) {
        return true;
      }
      code.value = RandomToken.gen({ length: 16, seed: 'n' }) as string;
      room.value = joinRoom(
        { appId: `${appStatic.appId}_${appStatic.version}` },
        code.value
      );
      const [sendRemoteError, getRemoteError] =
        room.value.makeAction('remoteError');
      const [sendDone] = room.value.makeAction('done');
      const [, getRegister] = room.value.makeAction('register');
      const [, getStartTransfer] = room.value.makeAction('startTrans');
      const [, getRunTransfer, processRunTransfer] =
        room.value.makeAction('runTrans');
      const [, getDisconnect] = room.value.makeAction('disconnect');
      getRemoteError(async (msg) => {
        error.value = msg as RemoteError;
      });
      getRegister(async (msg, peerId) => {
        const { cid } = msg as RemoteMsgRegister;
        connectedClients.value.cids.push(cid);
        connectedClients.value.peerIdMap[cid] = peerId;
        connectedClients.value.statusMap[cid] = 'ready';
        sendDone({ action: 'register' }, peerId);
      });
      getStartTransfer(async (msg, peerId) => {
        const { cid, fileInfo } = msg as RemoteMsgTransfer;
        if (!checkRegister('startTrans', cid, peerId)) {
          return;
        }
        if (Item.isText(fileInfo.format)) {
          const result = await Item.createItemByText(
            fileInfo.fileString || '',
            'receiver',
            'done'
          );
          if (result.error) {
            const errMsg = { action: 'startTrans', code: result.error.code };
            sendRemoteError(errMsg);
            error.value = errMsg;
            return;
          }
          progressMap.value[fileInfo.tUid] = 0;
          delete progressMap.value[fileInfo.tUid];
        } else {
          const result = await Item.createItemByFileInfo(fileInfo);
          if (result.error) {
            const errMsg = { action: 'startTrans', code: result.error.code };
            sendRemoteError(errMsg);
            error.value = errMsg;
            return;
          }
          progressMap.value[fileInfo.tUid] = 0;
        }
        sendDone({ action: 'startTrans', tUid: fileInfo.tUid });
      });
      getRunTransfer(async (buffer, peerId, metadata) => {
        const { cid, fileInfo } = metadata as RemoteMsgTransfer;
        if (!checkRegister('runTrans', cid, peerId)) {
          return;
        }
        const item = await Item.fetchByTUid('receiver', fileInfo.tUid);
        if (!item) {
          return;
        }
        item.putChunk((buffer as Uint8Array).buffer);
        const result = await item.switchStatus('done');
        if (result.error) {
          const errMsg = { action: 'runTrans', code: result.error.code };
          sendRemoteError(errMsg);
          error.value = errMsg;
        } else {
          connectedClients.value.statusMap[cid] = 'ready';
          if (progressMap.value[fileInfo.tUid] !== undefined) {
            delete progressMap.value[fileInfo.tUid];
          }
          sendDone({ action: 'runTrans', tUid: fileInfo.tUid });
        }
      });
      processRunTransfer(async (percent, _, metadata) => {
        const { cid, fileInfo } = metadata as RemoteMsgTransfer;
        connectedClients.value.statusMap[cid] = 'inAction';
        progressMap.value[fileInfo.tUid] = percent * 100;
      });
      getDisconnect(async (msg, peerId) => {
        const { cid } = msg as RemoteMsgCid;
        if (!checkRegister('disconnect', cid, peerId)) {
          return;
        }
        connectedClients.value.cids = connectedClients.value.cids.filter(
          (cCid) => cCid !== cid
        );
        delete connectedClients.value.peerIdMap[cid];
        delete connectedClients.value.statusMap[cid];
      });
      room.value.onPeerJoin((peerId) => {
        isConnected.value = true;
        sendDone({ action: 'connect' }, peerId);
      });
      room.value.onPeerLeave(() => {
        if (room.value && Object.keys(room.value.getPeers()).length === 0) {
          isConnected.value = false;
        }
      });
      return true;
    }

    async function disconnect() {
      if (!room.value) {
        return;
      }
      const [sendRemoteClose] = room.value.makeAction('remoteClose');
      sendRemoteClose(null);
      connectedClients.value = {
        cids: [],
        peerIdMap: {},
        statusMap: {}
      };
    }

    async function closeRemote() {
      if (!room.value) {
        return;
      }
      await disconnect();
      room.value.leave();
      room.value = null;
    }

    function myPeerId(): PeerId {
      return selfId;
    }

    function status(): ReceiverStatus {
      if (!room.value) {
        return 'none';
      }
      return isConnected.value ? 'connected' : 'ready';
    }

    function clientStatus(cid: ClientId): SenderStatus {
      return connectedClients.value.statusMap[cid];
    }

    function checkRegister(
      action: string,
      cid: ClientId,
      peerId: PeerId
    ): boolean {
      if (!room.value) {
        return false;
      }
      const [sendRemoteError] = room.value.makeAction('remoteError');
      if (!connectedClients.value.cids.includes(cid)) {
        sendRemoteError({ action, code: 'register_required' });
        return false;
      }
      if (connectedClients.value.peerIdMap[cid] !== peerId) {
        sendRemoteError({ action, code: 'register_required' });
        return false;
      }
      return true;
    }

    return {
      code,
      error,
      connectedClients,
      progressMap,
      openRemote,
      disconnect,
      closeRemote,
      myPeerId,
      status,
      clientStatus
    };
  }
);
