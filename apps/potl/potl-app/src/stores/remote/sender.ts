import { ref } from 'vue';
import { defineStore } from 'pinia';
import { joinRoom } from 'trystero';
import type { Room } from 'trystero';
import * as RandomToken from '@sibevin/random-token';
import appStatic from '../../static/app.json';
import { Item } from '../../models/item';
import { TransferUid } from '../../models/item-types';
import type { ClientId, RemoteError, RemoteMsgDone, TransferInfo } from './def';

type ClientStatus = 'none' | 'connected' | 'ready' | 'inAction';

export const useRemoteSenderStore = defineStore(
  'pybk-remote-sender-store',
  () => {
    const code = ref<string | null>(null);
    const room = ref<Room | null>(null);
    const clientId = ref<ClientId | null>(null);
    const clientStatus = ref<ClientStatus>('none');
    const error = ref<RemoteError | null>(null);
    const receiverPeerId = ref<string | null>(null);
    const transferInfoMap = ref<Record<TransferUid, TransferInfo>>({});

    function connect(connectCode: string): boolean {
      if (!connectCode) {
        error.value = { action: 'connect', code: 'code_required' };
        return false;
      }
      try {
        room.value = joinRoom(
          { appId: `${appStatic.appId}_${appStatic.version}` },
          connectCode
        );
      } catch (roomError) {
        error.value = {
          action: 'connect',
          code: 'room_error',
          extra: { error: (roomError as Error).message }
        };
        return false;
      }
      const [, getRemoteError] = room.value.makeAction('remoteError');
      const [, getDone] = room.value.makeAction('done');
      const [, getRemoteClose] = room.value.makeAction('remoteClose');
      const [sendRegister] = room.value.makeAction('register');
      getRemoteError((msg) => {
        error.value = msg as RemoteError;
      });
      getDone(async (msg, peerId) => {
        const doneMsg = msg as RemoteMsgDone;
        if (doneMsg.action === 'connect') {
          clientId.value = genRemoteId();
          receiverPeerId.value = peerId;
          clientStatus.value = 'connected';
          sendRegister({ cid: clientId.value }, peerId);
        }
        if (doneMsg.action === 'register') {
          clientStatus.value = 'ready';
        }
        if (doneMsg.action === 'startTrans' && doneMsg.tUid) {
          const item = await Item.fetchByTUid('sender', doneMsg.tUid);
          if (!item) {
            return;
          }
          clientStatus.value = 'inAction';
          if (item.isText) {
            await item.switchStatus('done');
            delete transferInfoMap.value[doneMsg.tUid];
            clientStatus.value = 'ready';
          } else {
            const bufferResult = await item.readFileBuffer();
            const transferInfo = transferInfoMap.value[doneMsg.tUid];
            if (transferInfo && bufferResult.target && room.value) {
              const [sendRunTransfer] = room.value.makeAction('runTrans');
              sendRunTransfer(
                bufferResult.target,
                receiverPeerId.value,
                {
                  cid: clientId.value,
                  fileInfo: transferInfo.fileInfo
                },
                (percent) => {
                  transferInfo.progress = percent * 100;
                }
              );
            }
          }
        }
        if (doneMsg.action === 'runTrans' && doneMsg.tUid) {
          const item = await Item.fetchByTUid('sender', doneMsg.tUid);
          if (!item) {
            return;
          }
          await item.switchStatus('done');
          delete transferInfoMap.value[doneMsg.tUid];
          clientStatus.value = 'ready';
        }
      });
      getRemoteClose(() => {
        resetClient();
      });
      return true;
    }

    async function sendItem(item: Item): Promise<boolean> {
      if (!room.value) {
        error.value = { action: 'sendItem', code: 'connect_first' };
        return false;
      }
      await item.switchStatus('processing');
      const [sendStartTransfer] = room.value.makeAction('startTrans');
      transferInfoMap.value[item.tUid] = {
        fileInfo: item.fileInfo,
        progress: 0
      };
      sendStartTransfer({ cid: clientId.value, fileInfo: item.fileInfo });
      return true;
    }

    function disconnect(): boolean {
      if (!room.value) {
        error.value = { action: 'login', code: 'connect_first' };
        return false;
      }
      const [sendDisconnect] = room.value.makeAction('disconnect');
      sendDisconnect({ cid: clientId.value }, receiverPeerId.value);
      resetClient();
      return true;
    }

    function resetClient() {
      if (room.value) {
        room.value.leave();
      }
      room.value = null;
      clientId.value = null;
      clientStatus.value = 'none';
    }

    function resetError() {
      error.value = null;
    }

    function genRemoteId(): string {
      return RandomToken.gen({ length: 6, casing: 'upper' });
    }

    function fetchTransferInfo(item: Item): TransferInfo | undefined {
      return transferInfoMap.value[item.tUid];
    }

    function available(): boolean {
      return (
        clientStatus.value === 'ready' || clientStatus.value === 'inAction'
      );
    }

    return {
      code,
      error,
      clientId,
      clientStatus,
      receiverPeerId,
      transferInfoMap,
      connect,
      sendItem,
      disconnect,
      resetError,
      fetchTransferInfo,
      available
    };
  }
);
