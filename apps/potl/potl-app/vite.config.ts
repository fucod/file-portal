/// <reference types='vitest' />
import { PluginOption, defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { nxViteTsPaths } from '@nx/vite/plugins/nx-tsconfig-paths.plugin';
import { VitePWA } from 'vite-plugin-pwa';
import * as appStatic from './src/static/app.json';
import { visualizer } from 'rollup-plugin-visualizer';

export default defineConfig({
  root: __dirname,
  cacheDir: '../../../node_modules/.vite/apps/potl/potl-app',
  publicDir: './public',

  server: {
    port: 4200,
    host: 'localhost'
  },

  preview: {
    port: 4300,
    host: 'localhost'
  },

  plugins: [
    vue(),
    nxViteTsPaths(),
    visualizer() as PluginOption,
    VitePWA({
      includeAssets: [
        'favicon.ico',
        'logo-main.svg',
        'platform/pwa/logo-mask-icon.svg',
        'assets/images/*.svg'
      ],
      manifest: {
        name: appStatic.name,
        short_name: appStatic.shortName,
        description: appStatic.description,
        theme_color: appStatic.mainColor,
        icons: [
          {
            src: 'platform/pwa/pwa-16x16.png',
            sizes: '16x16',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/pwa-32x32.png',
            sizes: '32x32',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/pwa-48x48.png',
            sizes: '48x48',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/pwa-64x64.png',
            sizes: '64x64',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/pwa-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/pwa-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/pwa-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/pwa-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/pwa-1024x1024.png',
            sizes: '1024x1024',
            type: 'image/png'
          },
          {
            src: 'platform/pwa/maskable-icon-512x512.png',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'maskable'
          }
        ]
      },
      registerType: 'autoUpdate',
      devOptions: {
        enabled: true
      }
    })
  ],

  // Uncomment this if you are using workers.
  // worker: {
  //  plugins: [ nxViteTsPaths() ],
  // },

  build: {
    outDir: '../../../dist/apps/potl/potl-app',
    reportCompressedSize: true,
    commonjsOptions: {
      transformMixedEsModules: true
    }
  },

  test: {
    globals: true,
    cache: {
      dir: '../../../node_modules/.vitest/apps/potl/potl-app'
    },
    environment: 'jsdom',
    include: ['src/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],

    reporters: ['default'],
    coverage: {
      reportsDirectory: '../../../coverage/apps/potl/potl-app',
      provider: 'v8'
    }
  }
});
