import Dexie from 'dexie';
import type { Table } from 'dexie';
import type { ConfigExistingDoc } from '../models/config-types';

export class ConfigDb extends Dexie {
  configs!: Table<ConfigExistingDoc, string>;

  constructor() {
    super('ConfigDb');
    this.version(1).stores({
      configs: '&uid'
    });
  }
}

export const configDb = new ConfigDb();
