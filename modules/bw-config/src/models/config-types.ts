import { ZodSchema, z } from 'zod';
import type { UidAttrs, UidParams } from '@base-workspace/bw-model';
import { UidSchema } from '@base-workspace/bw-model';

export type ConfigAttrs = {
  locale: string;
  fontSize: string;
  timeZone: string;
  theme: string;
};

export type ConfigParams = Partial<ConfigAttrs>;

export type ConfigDoc = ConfigAttrs & UidParams;

export type ConfigDocParams = Partial<ConfigDoc>;

export type ConfigExistingDoc = ConfigAttrs & UidAttrs;

export const VALIDATION_MAP: {
  [key in keyof ConfigDocParams]?: ZodSchema;
} = {
  uid: UidSchema.optional(),
  locale: z.string(),
  fontSize: z.string(),
  timeZone: z.string(),
  theme: z.string()
};
