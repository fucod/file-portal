import { DEFAULT_LOCALE } from '@base-workspace/bw-locale';
import { DEFAULT_THEME } from '@base-workspace/bw-theme';
import { DEFAULT_TIMEZONE } from '@base-workspace/bw-time-zone';
import { DEFAULT_FONT_SIZE } from '@base-workspace/bw-font-size';
import type { BaseModel, SaveResult, Uid } from '@base-workspace/bw-model';
import {
  handleModelError,
  validateParams,
  buildSaveError,
  normalizeDoc
} from '@base-workspace/bw-model';
import { genUid } from '@base-workspace/bw-model';
import { configDb } from '../db/config';
import type {
  ConfigAttrs,
  ConfigParams,
  ConfigDoc,
  ConfigDocParams,
  ConfigExistingDoc
} from './config-types';
import { VALIDATION_MAP } from './config-types';

export const DEFAULT_ATTRS: ConfigAttrs = {
  locale: DEFAULT_LOCALE,
  fontSize: DEFAULT_FONT_SIZE,
  timeZone: DEFAULT_TIMEZONE as string,
  theme: DEFAULT_THEME
};

type ConfigSaveResult = SaveResult<Config>;

export class Config implements BaseModel<Config, ConfigDoc> {
  doc: ConfigDoc;

  public constructor(doc: ConfigDocParams = {}) {
    this.doc = Object.assign({}, DEFAULT_ATTRS, doc);
  }

  get uid(): Uid | undefined {
    return this.doc.uid;
  }

  get isStored(): boolean {
    return this.uid !== undefined;
  }

  static async fetchCurrentConfig(): Promise<Config | null> {
    const docs = await configDb.configs.limit(1).toArray();
    if (docs.length === 0) {
      return null;
    }
    return new Config(docs[0]);
  }

  static async fetch(uid: string): Promise<Config | null> {
    const doc = await configDb.configs.where({ uid }).first();
    if (!doc) {
      return null;
    }
    return new Config(doc);
  }

  public assign(params: ConfigParams): Config {
    this.doc = Object.assign(this.doc, params);
    return this;
  }

  public async save(): Promise<ConfigSaveResult> {
    let docToSave = Object.assign({}, this.doc);
    let action = 'update';
    if (!this.doc.uid) {
      const idy = await Config.genUniqueIdy();
      docToSave = Object.assign(docToSave, idy);
      action = 'create';
    }
    const result = validateParams(docToSave, VALIDATION_MAP);
    if (!result.gain) {
      return buildSaveError(result.reason);
    }
    let errorCode: string | undefined;
    const normalizedDoc = normalizeDoc(docToSave) as ConfigExistingDoc;
    await configDb.configs.put(normalizedDoc).catch(function (error) {
      handleModelError(error, (code: string) => {
        errorCode = code;
      });
    });
    if (errorCode) {
      return buildSaveError(errorCode);
    } else {
      this.doc = docToSave;
      return { target: this, action };
    }
  }

  static async genUniqueIdy(): Promise<{
    uid: string;
  }> {
    let uid, foundUid;
    do {
      uid = genUid();
      foundUid = await configDb.configs.where({ uid }).first();
    } while (foundUid);
    return { uid };
  }
}
