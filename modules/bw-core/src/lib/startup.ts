import type { Composer } from 'vue-i18n';
import type { FontSize } from '@base-workspace/bw-font-size';
import { updateRootFontSize } from '@base-workspace/bw-font-size';
import { detectLocalLocale } from '@base-workspace/bw-locale';
import { detectLocalTheme } from '@base-workspace/bw-theme';
import { detectLocalTz } from '@base-workspace/bw-time-zone';
import { Config } from '@base-workspace/bw-config';
import { useCoreStore } from '../store/core';
import type { BuildMetadata } from '../types';

export function checkReady(): boolean {
  const appStore = useCoreStore();
  return appStore.isReady;
}

export async function init(
  coreStore: ReturnType<typeof useCoreStore>,
  i18n: Composer,
  buildMetadata: BuildMetadata
): Promise<void> {
  if (coreStore.isReady) {
    return;
  }
  let config = await Config.fetchCurrentConfig();
  if (!config) {
    config = buildConfigWithLocalDefaults(i18n, buildMetadata);
  }
  coreStore.config = config;
  const { locale } = i18n;
  locale.value = coreStore.config.doc.locale;
  updateRootFontSize(coreStore.config.doc.fontSize as FontSize);
  coreStore.isReady = true;
}

function buildConfigWithLocalDefaults(
  i18n: Composer,
  buildMetadata: BuildMetadata
): Config {
  return new Config({
    locale: buildMetadata.locale || detectLocalLocale(i18n),
    theme: detectLocalTheme(),
    timeZone: detectLocalTz() as string
  });
}
