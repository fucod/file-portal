import { ref } from 'vue';
import { defineStore } from 'pinia';
import { Config } from '@base-workspace/bw-config';

export const useCoreStore = defineStore('core', () => {
  const isReady = ref<boolean>(false);
  const config = ref<Config>(new Config());
  const currentPageCode = ref<string>('entry');

  return { config, isReady, currentPageCode };
});
