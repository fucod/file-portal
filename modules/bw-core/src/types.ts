export type BuildMetadata = {
  plan: string;
  locale?: string;
};
