const defaultTheme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
  safelist: [
    {
      pattern:
        /(text|bg)-(primary|secondary|accent|neutral|info|success|warning|error|base-100|base-200|base-300|base)(-content|)/
    },
    { pattern: /border-(r|l|t|b)-(0|4)/ },
    { pattern: /justify-(start|center|end|between|stretch)/ },
    { pattern: /items-(center|stretch)/ },
    { pattern: /grid-cols-(1|2|3|4|5|6|7|8|9|10|11|12|none)/ }
  ],
  theme: {
    extend: {
      height: {
        screen: 'calc(var(--vh) * 100)',
        v80: 'calc(var(--vh) * 80)'
      },
      maxHeight: {
        screen: 'calc(var(--vh) * 100)',
        v15: 'calc(var(--vh) * 15)',
        v30: 'calc(var(--vh) * 30)',
        v60: 'calc(var(--vh) * 60)'
      },
      maxWidth: {
        screen: '100vw'
      },
      fontFamily: {
        sans: ['Noto Sans', ...defaultTheme.fontFamily.sans],
        serif: ['Noto Serif', ...defaultTheme.fontFamily.serif],
        'ui-mono': 'Fira Mono'
      },
      boxShadow: {
        top: '0 -1px 3px 0 rgb(0 0 0 / 0.1), 0 -1px 2px -1px rgb(0 0 0 / 0.1);',
        left: '-1px 0 3px 0 rgb(0 0 0 / 0.1), -1px 0 2px -1px rgb(0 0 0 / 0.1);',
        right: '1px 0 3px 0 rgb(0 0 0 / 0.1), 1px 0 2px -1px rgb(0 0 0 / 0.1);'
      }
    },
    borderColor: ({ theme }) => ({
      ...theme('colors'),
      DEFAULT: theme('colors.base-300', 'currentColor')
    })
  },
  plugins: [require('daisyui')],
  daisyui: {
    darkTheme: 'black',
    themes: [
      {
        white: {
          'color-scheme': 'light',
          primary: '#374151',
          secondary: '#71717a',
          accent: '#54637c',
          neutral: '#181a2a',
          info: '#3abff8',
          success: '#36d399',
          warning: '#fbbd23',
          error: '#f87272',
          'neutral-content': '#edf2f7',
          'base-100': '#ffffff',
          'base-200': '#e6e6e6',
          'base-300': '#d9d9d9',
          'base-content': '#181a2a',
          '--rounded-box': '0.25rem',
          '--rounded-btn': '.2rem',
          '--rounded-badge': '.125rem',
          '--animation-btn': '0.2s',
          '--animation-input': '0',
          '--btn-focus-scale': '0.95',
          '--btn-text-case': 'none'
        }
      },
      {
        black: {
          'color-scheme': 'dark',
          primary: '#b2b2b2',
          secondary: '#545252',
          accent: '#939191',
          neutral: '#272626',
          'neutral-focus': '#343232',
          info: '#3abff8',
          success: '#36d399',
          warning: '#fbbd23',
          error: '#f87272',
          'base-100': '#000000',
          'base-200': '#3b3b3b',
          'base-300': '#4c4c4c',
          'base-content': '#edf2f7',
          '--rounded-box': '0.25rem',
          '--rounded-btn': '.2rem',
          '--rounded-badge': '.125rem',
          '--animation-btn': '0.2s',
          '--animation-input': '0',
          '--btn-focus-scale': '0.95',
          '--btn-text-case': 'none'
        }
      },
      {
        ocean: {
          'color-scheme': 'light',
          primary: '#1e3a8a',
          secondary: '#0369a1',
          accent: '#0d9488',
          neutral: '#181a2a',
          info: '#3abff8',
          success: '#36d399',
          warning: '#fbbd23',
          error: '#f87272',
          'neutral-content': '#edf2f7',
          'base-100': '#eff6ff',
          'base-200': '#ccdbf3',
          'base-300': '#bfdbfe',
          'base-400': '#bbc3d7',
          'base-content': '#101c44',
          '--rounded-box': '0.25rem',
          '--rounded-btn': '.2rem',
          '--rounded-badge': '.125rem',
          '--animation-btn': '0.2s',
          '--animation-input': '0',
          '--btn-focus-scale': '0.95',
          '--btn-text-case': 'none'
        }
      },
      {
        sakura: {
          'color-scheme': 'light',
          primary: '#7f1d1d',
          secondary: '#9a3412',
          accent: '#be123c',
          neutral: '#181a2a',
          info: '#3abff8',
          success: '#36d399',
          warning: '#fbbd23',
          error: '#f87272',
          'neutral-content': '#edf2f7',
          'base-100': '#fef2f2',
          'base-200': '#ffdede',
          'base-300': '#fecaca',
          'base-400': '#e5b6b6',
          'base-content': '#3f1d1d',
          '--rounded-box': '0.25rem',
          '--rounded-btn': '.2rem',
          '--rounded-badge': '.125rem',
          '--animation-btn': '0.2s',
          '--animation-input': '0',
          '--btn-focus-scale': '0.95',
          '--btn-text-case': 'none'
        }
      },
      {
        forest: {
          'color-scheme': 'light',
          primary: '#064e3b',
          secondary: '#3f6212',
          accent: '#15803d',
          neutral: '#181a2a',
          info: '#3abff8',
          success: '#36d399',
          warning: '#fbbd23',
          error: '#f87272',
          'neutral-content': '#edf2f7',
          'base-100': '#f0fdf4',
          'base-200': '#95deb0',
          'base-300': '#7bb791',
          'base-400': '#6fa582',
          'base-content': '#043025',
          '--rounded-box': '0.25rem',
          '--rounded-btn': '.2rem',
          '--rounded-badge': '.125rem',
          '--animation-btn': '0.2s',
          '--animation-input': '0',
          '--btn-focus-scale': '0.95',
          '--btn-text-case': 'none'
        }
      },
      {
        antique: {
          'color-scheme': 'light',
          primary: '#78350f',
          secondary: '#a16207',
          accent: '#d97706',
          neutral: '#181a2a',
          info: '#2794c1',
          success: '#32956f',
          warning: '#d1950b',
          error: '#e32d2d',
          'neutral-content': '#edf2f7',
          'base-100': '#fef3c7',
          'base-200': '#f9d25f',
          'base-300': '#e2bf56',
          'base-400': '#cbac4d',
          'base-content': '#351706',
          '--rounded-box': '0.25rem',
          '--rounded-btn': '.2rem',
          '--rounded-badge': '.125rem',
          '--animation-btn': '0.2s',
          '--animation-input': '0',
          '--btn-focus-scale': '0.95',
          '--btn-text-case': 'none'
        }
      }
    ]
  }
};
