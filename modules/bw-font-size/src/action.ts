import type { FontSize } from './def';
import { FONT_SIZE_MAP } from './def';

export function updateRootFontSize(fontSize: FontSize): void {
  const root = document.querySelector(':root');
  if (root) {
    (root as HTMLElement).style.fontSize = `${FONT_SIZE_MAP[fontSize]}px`;
  }
}
