export const FONT_SIZES = ['xs', 's', 'm', 'l', 'xl'] as const;
export type FontSize = (typeof FONT_SIZES)[number];

export const DEFAULT_FONT_SIZE = 'm';

export const FONT_SIZE_MAP: Record<FontSize, number> = {
  xs: 10,
  s: 12,
  m: 16,
  l: 18,
  xl: 24
};
