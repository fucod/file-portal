export { default as FontSizeSelector } from './components/FontSizeSelector.vue';
export { default as FontSizeSelectorPanel } from './components/FontSizeSelectorPanel.vue';
export type { FontSize } from './def';
export { FONT_SIZES, DEFAULT_FONT_SIZE, FONT_SIZE_MAP } from './def';
export { updateRootFontSize } from './action';
