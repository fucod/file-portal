import type { IconifyIcon } from '@iconify/vue';
import { fetchIcon } from './fetch-icon';

const MDI_SIZE = 24;
const PH_SIZE = 256;
const GI_SIZE = 512;

export function getIcon(prefix: string, icon: string): IconifyIcon {
  if (prefix === 'mdi') {
    return {
      ...fetchIcon(prefix, icon),
      ...{ width: MDI_SIZE, height: MDI_SIZE }
    };
  }
  if (prefix === 'ph') {
    return {
      ...fetchIcon(prefix, icon),
      ...{ width: PH_SIZE, height: PH_SIZE }
    };
  }
  if (prefix === 'gi') {
    return {
      ...fetchIcon(prefix, icon),
      ...{ width: GI_SIZE, height: GI_SIZE }
    };
  }
  return {
    ...fetchIcon(prefix, icon),
    ...{ width: MDI_SIZE, height: MDI_SIZE }
  };
}
