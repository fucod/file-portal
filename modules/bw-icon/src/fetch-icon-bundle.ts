import { default as bundleIcons } from './icon-bundle.json';

export function fetchIcon(prefix: string, icon: string): { body: string } {
  const bundleIconKey = `${prefix}_${icon}` as keyof typeof bundleIcons;
  const result = bundleIcons[bundleIconKey];
  if (result) {
    return result;
  } else {
    // NOTE: Show close-box-outline icon when the target icon is not found in bundled icons
    return {
      body: '<path fill="currentColor" d="M19 3H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V5a2 2 0 0 0-2-2m0 16H5V5h14zM17 8.4L13.4 12l3.6 3.6l-1.4 1.4l-3.6-3.6L8.4 17L7 15.6l3.6-3.6L7 8.4L8.4 7l3.6 3.6L15.6 7z"/>'
    };
  }
}
