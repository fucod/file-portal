import { icons as phIcons } from '@iconify-json/ph';
import { icons as mdiIcons } from '@iconify-json/mdi';
import { icons as giIcons } from '@iconify-json/game-icons';

export function fetchIcon(prefix: string, icon: string): { body: string } {
  if (prefix === 'mdi') {
    return mdiIcons.icons[icon];
  }
  if (prefix === 'ph') {
    return phIcons.icons[icon];
  }
  if (prefix === 'gi') {
    return giIcons.icons[icon];
  }
  return mdiIcons.icons[icon];
}
