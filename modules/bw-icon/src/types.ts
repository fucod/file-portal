import type { IconifyIcon } from '@iconify/vue';

export type IconInfo = {
  icon: IconifyIcon;
  size?: number;
  scale?: { w?: number; h?: number };
  flip?: { h?: boolean; v?: boolean };
  offset?: { x?: string; y?: string };
  style?: Record<string, string>;
};
