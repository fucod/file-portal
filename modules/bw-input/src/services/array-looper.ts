export function nextIndex<T>(arr: readonly T[], currentIndex?: number): number {
  if (arr.length === 0) {
    return -1;
  }
  if (currentIndex === undefined) {
    return 0;
  }
  if (currentIndex < 0 || currentIndex >= arr.length - 1) {
    return 0;
  } else {
    return currentIndex + 1;
  }
}

export function prevIndex<T>(arr: readonly T[], currentIndex?: number): number {
  if (arr.length === 0) {
    return -1;
  }
  if (currentIndex === undefined) {
    return arr.length - 1;
  }
  if (currentIndex <= 0 || currentIndex >= arr.length) {
    return arr.length - 1;
  } else {
    return currentIndex - 1;
  }
}

export function nextEntry<T>(arr: readonly T[], currentEntry?: T): T {
  if (!currentEntry) {
    return arr[0];
  }
  const foundIndex = arr.indexOf(currentEntry);
  const index = nextIndex(arr, foundIndex);
  return arr[index];
}

export function prevEntry<T>(arr: readonly T[], currentEntry?: T): T {
  if (!currentEntry) {
    return arr[arr.length - 1];
  }
  const foundIndex = arr.indexOf(currentEntry);
  const index = prevIndex(arr, foundIndex);
  return arr[index];
}

export function moveEntryToPrev<T>(
  arr: readonly T[],
  srcIndex: number
): { arr: T[]; index: number } {
  const destIndex = srcIndex > 0 ? srcIndex - 1 : arr.length - 1;
  return moveEntryToIndex(arr, srcIndex, destIndex);
}

export function moveEntryToNext<T>(
  arr: readonly T[],
  srcIndex: number
): { arr: T[]; index: number } {
  const destIndex = srcIndex < arr.length - 1 ? srcIndex + 1 : 0;
  return moveEntryToIndex(arr, srcIndex, destIndex);
}

export function moveEntryToIndex<T>(
  arr: readonly T[],
  srcIndex: number,
  destIndex: number
): { arr: T[]; index: number } {
  if (srcIndex === destIndex) {
    return { arr: [...arr], index: srcIndex };
  }
  if (
    srcIndex < 0 ||
    srcIndex >= arr.length ||
    destIndex < 0 ||
    destIndex >= arr.length
  ) {
    return { arr: [...arr], index: 0 };
  } else {
    const modifiedArr = [...arr];
    modifiedArr.splice(destIndex, 0, modifiedArr.splice(srcIndex, 1)[0]);
    return { arr: modifiedArr, index: destIndex };
  }
}

export function sortByIndexes<T>(
  arr: readonly T[],
  sortedIndexes: number[]
): T[] {
  if (sortedIndexes.length !== arr.length) {
    return [...arr];
  }
  const sortedArr: T[] = [];
  sortedIndexes.forEach((newIndex) => {
    sortedArr.push(arr[newIndex]);
  });
  return sortedArr;
}
