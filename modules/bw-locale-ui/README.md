# bw-locale-ui

Provide locale(i18n)-related UI components.

# Implementation details

- It is separated from `bw-locale` to avoid cycle dependency between modules.
