export type CoreLocaleMapType = {
  [key: string]: {
    locale: {
      name: string;
    };
    core: Record<string, never>;
  };
};

export const CORE_LOCALE_MAP: CoreLocaleMapType;
