import core from './core/zh-CN.json' assert { type: 'json' };

export default {
  locale: {
    name: '简体中文',
    shortName: '简中',
    title: '语言'
  },
  core
};
