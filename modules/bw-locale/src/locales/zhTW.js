import core from './core/zh-TW.json' assert { type: 'json' };

export default {
  locale: {
    name: '繁體中文',
    shortName: '繁中',
    title: '語言'
  },
  core
};
