import { genSn, showSn, showSnDt } from './serial-number';

describe('genSn', () => {
  it('generates an object with a date and a random number', () => {
    const sn = genSn();
    expect(sn).toHaveProperty('dt');
    expect(sn.dt).toBeInstanceOf(Date);
    expect(sn).toHaveProperty('num');
    expect(sn.num).toBeGreaterThanOrEqual(10000000);
    expect(sn.num).toBeLessThanOrEqual(99999999);
  });
});

describe('showSn', () => {
  it('returns a string with formatted date and number', () => {
    const sn = {
      dt: new Date('2024-03-24T15:33:41.000Z'),
      num: 12345678
    };
    const result = showSn(sn);
    expect(result).toBe('20240324-1533-41000-12345678');
  });
});

describe('showSnDt', () => {
  it('returns a string with formatted date and number', () => {
    const result = showSnDt(new Date('2024-03-24T15:33:41.000Z'));
    expect(result).toBe('20240324-1533-41000');
  });
});
