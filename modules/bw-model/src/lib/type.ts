import type { IconifyIcon } from '@iconify/vue';
export type Uid = string;

export type SortOpt = {
  field: string;
  order?: 'asc' | 'desc';
};

export type SortData = {
  icon: IconifyIcon;
  sort: SortOpt;
};

export type SaveError = {
  code: string;
  raw?: Error;
};

export type OkResult<T> = {
  target: T;
  error?: SaveError;
};

export type ErrorResult<T> = {
  target?: T;
  error: SaveError;
};

export type SaveResult<T> = {
  action?: string;
} & (OkResult<T> | ErrorResult<T>);

export type CheckFieldResult = {
  gain: boolean;
  reason: string;
  field: string;
};

export interface BaseModel<ModelKlass, ModelDoc> {
  doc: ModelDoc;
  uid: Uid | undefined;
  isStored: boolean;
  save: () => Promise<SaveResult<ModelKlass>>;
}

export type UidAttrs = {
  uid: Uid;
};

export type UidParams = Partial<UidAttrs>;
