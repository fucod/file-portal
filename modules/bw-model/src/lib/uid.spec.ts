import { genUid, UidRegex, UidSchema } from './uid';

describe('genUid()', () => {
  it('generates a UID with timestamp and random token', () => {
    const uid = genUid();
    expect(uid).toMatch(UidRegex);
  });
});

describe('UidRegex', () => {
  it('validates a correctly formatted UID', () => {
    const validUid = '20240101010101123abcdefgh12345678';
    expect(validUid).toMatch(UidRegex);
  });

  it('rejects an incorrectly formatted UID', () => {
    const invalidUid = '2024-01-01T01:01:01.123Zabcdefgh12345678';
    expect(invalidUid).not.toMatch(UidRegex);
  });
});

describe('UidSchema', () => {
  it('validates a correctly formatted UID', () => {
    const validUid = '20240101010101123abcdefgh12345678';
    expect(UidSchema.safeParse(validUid).success).toBe(true);
  });

  it('rejects an incorrectly formatted UID', () => {
    const invalidUid = '2024-01-01T01:01:01.123Zabcdefgh12345678';
    expect(UidSchema.safeParse(invalidUid).success).toBe(false);
  });
});
