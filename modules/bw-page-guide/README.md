# bw-page-guide

A module to provide page-related types and functions.

# What & Why

- Use a `PageGuideData[]` array (called `PageDef`) to define basic information such as title, icon, routes for each pages.
- `PageDef` is used to generate page headers, links, menu entries for pages.

# How

## An example of `PageDef`

`<app-folder>/src/services/page-def.ts`

```typescript
import { PageGuideData } from '@base-workspace/bw-page-guide';
import { getIcon } from '@base-workspace/bw-icon';

export const PAGES: PageGuideData[] = [
  {
    code: 'demo',
    route: { name: 'demo' },
    nameLk: 'pages.demo.title',
    iconInfo: {
      icon: getIcon('mdi', 'view-dashboard'),
    },
  },
  {
    code: 'style',
    route: { name: 'style' },
    nameLk: 'pages.style.title',
    iconInfo: {
      icon: getIcon('mdi', 'palette'),
    },
  },
  {
    code: 'about',
    route: { name: 'about' },
    nameLk: 'pages.about.title',
    iconInfo: {
      icon: getIcon('mdi', 'star'),
    },
  },
  {
    code: 'settings',
    route: { name: 'settings' },
    nameLk: 'pages.settings.title',
    iconInfo: {
      icon: getIcon('mdi', 'cog'),
    },
  },
  {
    code: 'settings-format',
    parent: 'settings',
    route: { name: 'settings', query: { tab: 'settings-format' } },
    nameLk: 'pages.settings.tab.format',
    iconInfo: {
      icon: getIcon('mdi', 'form-textbox'),
    },
  },
  {
    code: 'settings-display',
    parent: 'settings',
    route: { name: 'settings', query: { tab: 'settings-display' } },
    nameLk: 'pages.settings.tab.display',
    iconInfo: {
      icon: getIcon('mdi', 'view-dashboard-edit'),
    },
  },
  {
    code: 'dev',
    route: { name: 'dev' },
    nameLk: 'pages.dev.title',
    iconInfo: {
      icon: getIcon('mdi', 'tools'),
    },
  },
  {
    code: 'entry',
    route: { name: 'entry' },
    nameLk: 'app.service.home',
    iconInfo: {
      icon: getIcon('mdi', 'home'),
    },
  },
];
```

# Development

## Run tests

- `nx test bw-page-guide`

## Generate doc

- `nx gendoc bw-page-guide`
