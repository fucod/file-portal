import { DARK_THEMES } from './def';

export function isThemeDark(theme: string): boolean {
  return DARK_THEMES.includes(theme);
}
