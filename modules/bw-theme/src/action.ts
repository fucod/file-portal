import { DEFAULT_THEME, DEFAULT_DARK_THEME } from './def';

export function detectLocalTheme(): string {
  if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
    return DEFAULT_DARK_THEME;
  } else {
    return DEFAULT_THEME;
  }
}
