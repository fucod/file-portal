import timeZoneData from './data/geo.json';
import type { DateRange, TimeMark } from './def';
import { RangeParsedResult, TIME_MARK } from './def';

type TimeZoneData = {
  continent: string;
  city: string;
  longitude: number;
  latitude: number;
  labelAdjustment?: {
    x?: number;
    y?: number;
    anchor?: string;
  };
};

export type TimeZoneValue = keyof typeof timeZoneData;

export const TIME_ZONES = Object.keys(timeZoneData) as TimeZoneValue[];

export const DEFAULT_TIMEZONE: TimeZoneValue = 'Asia/Tokyo';

export function detectLocalTz(): TimeZoneValue {
  return (
    (Intl.DateTimeFormat().resolvedOptions().timeZone as TimeZoneValue) ||
    DEFAULT_TIMEZONE
  );
}

export function fetchTzData(timeZone: TimeZoneValue): TimeZoneData {
  return timeZoneData[timeZone];
}

export function fetchTzOffset(timeZone: TimeZoneValue): string {
  const partMap = buildDateFormatPartMap(new Date(), timeZone as string);
  return partMap['timeZoneName'];
}

export function formatDateWithTz(
  timeZone: string,
  date?: Date,
  options: {
    showSec?: boolean;
    dateOnly?: boolean;
  } = {}
): string {
  const givenDt = date || new Date();
  const partMap = buildDateFormatPartMap(givenDt, timeZone);
  if (options.dateOnly) {
    return `${partMap['year']}.${partMap['month']}.${partMap['day']}`;
  }
  const dtStr = `${partMap['year']}.${partMap['month']}.${partMap['day']} ${partMap['hour']}:${partMap['minute']}`;
  if (options.showSec) {
    return `${dtStr}:${partMap['second']}`;
  } else {
    return dtStr;
  }
}

export function getDayWithTz(timeZone: string, date?: Date): number {
  const givenDt = date || new Date();
  const dtWithTz = new Date(givenDt.toLocaleString('en-US', { timeZone }));
  return dtWithTz.getDay();
}

export function buildDateFormatPartMap(
  date: Date,
  timeZone: string
): Record<string, string> {
  const formatter = new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: false,
    timeZone,
    timeZoneName: 'longOffset',
    weekday: 'short'
  });
  const parts = formatter.formatToParts(date);
  const partMap: Record<string, string> = {};
  for (let i = 0; i < parts.length; i++) {
    const part = parts[i];
    if (part.type !== 'literal') {
      partMap[part.type] = part.value;
    }
  }
  return partMap;
}

const TIME_MARK_TO_DAYS: Record<TimeMark, number> = {
  d: 1,
  w: 7,
  m: 30,
  q: 90,
  y: 365
};

export function fetchRangeByCode(code: string, timeZone: string): DateRange {
  const baseDt = new Date();
  const parseResult = parseRange(code);
  if (!parseResult) {
    return {};
  }
  if (parseResult.kind === 'quick') {
    switch (parseResult.mark) {
      case 'd':
      case 'w':
        baseDt.setDate(
          baseDt.getDate() -
            parseResult.count * TIME_MARK_TO_DAYS[parseResult.mark]
        );
        break;
      case 'm':
        baseDt.setMonth(baseDt.getMonth() - parseResult.count, 1);
        break;
      case 'q':
        baseDt.setMonth(baseDt.getMonth() - parseResult.count * 3, 1);
        break;
      case 'y':
        baseDt.setFullYear(baseDt.getFullYear() - parseResult.count, 1);
        break;
    }
  }
  const partMap = buildDateFormatPartMap(baseDt, timeZone);
  const monthNum = parseInt(partMap['month'], 10);
  const from = structuredClone(baseDt);
  const to = structuredClone(baseDt);
  if (parseResult.kind === 'since') {
    from.setDate(
      from.getDate() - parseResult.count * TIME_MARK_TO_DAYS[parseResult.mark]
    );
  } else if (parseResult.kind === 'quick' || code === 'today') {
    switch (parseResult.mark) {
      case 'w':
        return fetchRangeByWeekOfYear(
          baseDt.getFullYear(),
          getWeekOfYear(baseDt)
        );
      case 'm':
        from.setMonth(baseDt.getMonth(), 1);
        to.setMonth(baseDt.getMonth() + 1, 1);
        from.setHours(0, 0, 0, 0);
        to.setHours(0, 0, 0, 0);
        break;
      case 'q':
        if (monthNum >= 1 && monthNum <= 3) {
          from.setFullYear(baseDt.getFullYear(), 0, 1);
          to.setFullYear(baseDt.getFullYear(), 3, 1);
        }
        if (monthNum >= 4 && monthNum <= 6) {
          from.setFullYear(baseDt.getFullYear(), 3, 1);
          to.setFullYear(baseDt.getFullYear(), 6, 1);
        }
        if (monthNum >= 7 && monthNum <= 9) {
          from.setFullYear(baseDt.getFullYear(), 6, 1);
          to.setFullYear(baseDt.getFullYear(), 9, 1);
        }
        if (monthNum >= 10 && monthNum <= 12) {
          from.setFullYear(baseDt.getFullYear(), 9, 1);
          to.setFullYear(baseDt.getFullYear(), 12, 1);
        }
        from.setHours(0, 0, 0, 0);
        to.setHours(0, 0, 0, 0);
        break;
      case 'y':
        from.setFullYear(baseDt.getFullYear(), 0, 1);
        from.setHours(0, 0, 0, 0);
        to.setFullYear(baseDt.getFullYear() + 1, 0, 1);
        to.setHours(0, 0, 0, 0);
        break;
      default: // d, today
        from.setHours(0, 0, 0, 0);
        to.setHours(23, 59, 59, 999);
    }
  } else {
    return {};
  }
  return { from, to };
}

export function fetchRangeByWeekOfYear(
  fullYear: number,
  weekOfYear: number
): DateRange {
  const yFirstDt = new Date(fullYear, 0, 1);
  const days = (weekOfYear - 1) * 7;
  const from = new Date(yFirstDt.setDate(yFirstDt.getDate() + days));
  const to = new Date(from);
  to.setDate(from.getDate() + 6);
  return { from, to };
}

export function getWeekOfYear(dt: Date): number {
  const yFirstDt = new Date(dt.getFullYear(), 0, 1);
  return Math.ceil(
    ((dt.getTime() - yFirstDt.getTime()) / 86400000 + yFirstDt.getDay() + 1) / 7
  );
}

export function fetchQuickDisplayByCode(
  code: string,
  timeZone: string
): string {
  if (code === 'all' || code === 'today') {
    return code;
  }
  const baseDt = new Date();
  const parseResult = parseRange(code);
  if (parseResult && parseResult.kind === 'quick') {
    baseDt.setDate(
      baseDt.getDate() - parseResult.count * TIME_MARK_TO_DAYS[parseResult.mark]
    );
    const partMap = buildDateFormatPartMap(baseDt, timeZone);
    const monthNum = parseInt(partMap['month'], 10);
    let qNum = 1;
    switch (parseResult.mark) {
      case 'd':
        return `${partMap['month']}.${partMap['day']}`;
      case 'w':
        return `${partMap['year']}.W${getWeekOfYear(baseDt)}`;
      case 'm':
        return `${partMap['year']}.${partMap['month']}`;
      case 'q':
        if (monthNum >= 1 && monthNum <= 3) {
          qNum = 1;
        }
        if (monthNum >= 4 && monthNum <= 6) {
          qNum = 2;
        }
        if (monthNum >= 7 && monthNum <= 9) {
          qNum = 3;
        }
        if (monthNum >= 10 && monthNum <= 12) {
          qNum = 4;
        }
        return `${partMap['year']}.Q${qNum}`;
      case 'y':
        return partMap['year'];
    }
  }
  return '';
}

export function parseRange(code: string): RangeParsedResult | null {
  if (code === 'all') {
    return null;
  }
  if (code === 'today') {
    return { kind: 'quick', mark: 'd', count: 0 };
  }
  const quickRange = parseQuickRange(code);
  if (quickRange) {
    return { ...quickRange, ...{ kind: 'quick' } };
  }
  const sinceRange = parseSinceRange(code);
  if (sinceRange) {
    return { ...sinceRange, ...{ kind: 'since' } };
  }
  return null;
}

function parseSinceRange(
  code: string
): null | { count: number; mark: TimeMark } {
  const match = code.match(/^(\d+)(\D+)$/);
  if (match && TIME_MARK.includes(match[2] as TimeMark)) {
    return { count: parseInt(match[1], 10), mark: match[2] as TimeMark };
  } else {
    return null;
  }
}

function parseQuickRange(
  code: string
): null | { count: number; mark: TimeMark } {
  const match = code.match(/^(\D+)(\d+)$/);
  if (match && TIME_MARK.includes(match[1] as TimeMark)) {
    return { count: parseInt(match[2], 10), mark: match[1] as TimeMark };
  } else {
    return null;
  }
}
