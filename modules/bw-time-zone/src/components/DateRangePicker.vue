<script setup lang="ts">
import { PropType, computed, ref, watch } from 'vue';
import { useI18n } from 'vue-i18n';
import { LocaleActor } from '@base-workspace/bw-locale';
import { IconInfo, IconPanel, getIcon } from '@base-workspace/bw-icon';
import { BlockPanel } from '@base-workspace/bw-ui';
import { LOCALE_MAP } from '../locales/index';
import '@vuepic/vue-datepicker/dist/main.css';
import { fetchRangeByCode, formatDateWithTz } from '../action';
import type {
  DateRange,
  QuickDateRange,
  SinceDateRange,
  DateRangeDirection
} from '../def';
import DateTimeSelectorDialog from './DateTimeSelectorDialog.vue';
import DateRangePanel from './DateRangePanel.vue';

const props = defineProps({
  rangeCode: {
    type: String,
    required: true
  },
  range: {
    type: Object as PropType<DateRange>,
    required: true
  },
  timeZone: {
    type: String,
    required: true
  },
  sinceRanges: {
    type: Array as PropType<SinceDateRange[]>,
    default: () => ['1d', '1w', '1m']
  },
  quickRange: {
    type: Object as PropType<QuickDateRange>,
    default: () => {
      return { d: 3, m: 3, y: 2 };
    }
  }
});

const emit = defineEmits<{
  select: [value: { range: DateRange; code: string }];
}>();

const i18n = useI18n({ useScope: 'local', messages: LOCALE_MAP });
const la = new LocaleActor('components.DateRangePicker', i18n.t);
const dtSelectorDialog = ref();

const inputDate = ref<Date | undefined>(undefined);
const inputDirection = ref<DateRangeDirection | null>(null);
const currentRangeCode = ref<string>(props.rangeCode);
const pickedRange = ref<DateRange>(props.range);
const customizedRange = ref<DateRange>(props.range);
const selectorRange = ref<DateRange>(props.range);

const pickerDiglogTitle = computed<string>(() => {
  if (inputDirection.value === 'from') {
    return la.t('.fromDate') as string;
  } else {
    return la.t('.toDate') as string;
  }
});

const pickerDiglogTitleIconInfo = computed<IconInfo>(() => {
  if (inputDirection.value === 'from') {
    return {
      icon: getIcon('mdi', 'ray-start'),
      size: 1.5
    };
  } else {
    return {
      icon: getIcon('mdi', 'ray-end'),
      size: 1.5
    };
  }
});

watch(
  () => props.rangeCode,
  (newValue) => {
    currentRangeCode.value = newValue;
  }
);

watch(
  () => currentRangeCode.value,
  (newValue) => {
    if (newValue === 'customized') {
      pickedRange.value = customizedRange.value;
    }
  }
);

function onDateTimePicked(dt: Date | null) {
  if (dt) {
    if (inputDirection.value === 'from') {
      dt.setHours(0, 0, 0, 0);
      pickedRange.value.from = dt;
      customizedRange.value.from = dt;
    } else if (inputDirection.value === 'to') {
      dt.setHours(23, 59, 59, 999);
      pickedRange.value.to = dt;
      customizedRange.value.to = dt;
    }
  } else {
    if (inputDirection.value === 'from') {
      pickedRange.value.from = undefined;
      customizedRange.value.from = undefined;
    } else if (inputDirection.value === 'to') {
      pickedRange.value.to = undefined;
      customizedRange.value.to = undefined;
    }
  }
  emit('select', { range: pickedRange.value, code: 'customized' });
}

function openPickerDialog(direction: DateRangeDirection) {
  inputDirection.value = direction;
  if (direction === 'from') {
    inputDate.value = customizedRange.value.from;
    selectorRange.value = { to: customizedRange.value.to };
  } else {
    inputDate.value = customizedRange.value.to;
    selectorRange.value = { from: customizedRange.value.from };
  }
  dtSelectorDialog.value.openDialog();
}

function onRangeCodeSelected(code: string): void {
  if (code === 'customized') {
    emit('select', { range: pickedRange.value, code: 'customized' });
    return;
  }
  const fetchResult = fetchRangeByCode(code, props.timeZone);
  if (fetchResult) {
    pickedRange.value = fetchResult;
    emit('select', { range: fetchResult, code });
  }
}
</script>

<template>
  <BlockPanel class="p-3">
    <template #title>
      <div class="text-secondary flex items-center gap-1">
        {{ la.t('.title') }}
      </div>
    </template>
    <div class="flex flex-col gap-2">
      <div class="flex items-center gap-2 flex-wrap">
        <label class="cursor-pointer flex items-center gap-1">
          <input
            class="radio radio-sm"
            type="radio"
            v-model="currentRangeCode"
            value="all"
            @input="onRangeCodeSelected('all')"
          />
          <DateRangePanel
            code="all"
            :time-zone="props.timeZone"
          ></DateRangePanel>
        </label>
        <label
          v-for="rg in sinceRanges"
          :key="rg"
          class="cursor-pointer flex items-center gap-1"
        >
          <input
            class="radio radio-sm"
            type="radio"
            v-model="currentRangeCode"
            :value="rg"
            @input="onRangeCodeSelected(rg)"
          />
          <DateRangePanel
            :code="rg"
            :time-zone="props.timeZone"
          ></DateRangePanel>
        </label>
      </div>
      <div class="flex items-center flex-wrap gap-1">
        <div
          class="w-6 h-6 mr-2 text-sm border rounded flex items-center justify-center"
        >
          {{ la.t('.mark.d') }}
        </div>
        <label class="mr-2 cursor-pointer flex items-center gap-1">
          <input
            class="radio radio-sm"
            type="radio"
            v-model="currentRangeCode"
            value="today"
            @input="onRangeCodeSelected('today')"
          />
          <DateRangePanel
            code="today"
            :time-zone="props.timeZone"
          ></DateRangePanel>
        </label>
        <template v-if="quickRange.d">
          <label
            v-for="i in quickRange.d"
            :key="`quickRange_d${i}`"
            class="mr-2 cursor-pointer flex items-center gap-1"
          >
            <input
              class="radio radio-sm"
              type="radio"
              v-model="currentRangeCode"
              :value="`d${i}`"
              @input="onRangeCodeSelected(`d${i}`)"
            />
            <DateRangePanel
              :code="`d${i}`"
              :time-zone="props.timeZone"
            ></DateRangePanel>
          </label>
        </template>
      </div>
      <div v-if="quickRange.w" class="flex items-center flex-wrap gap-1">
        <div
          class="w-6 h-6 mr-2 text-sm border rounded flex items-center justify-center"
        >
          {{ la.t('.mark.w') }}
        </div>
        <label
          v-for="i in quickRange.w"
          :key="`quickRange_w${i - 1}`"
          class="mr-2 cursor-pointer flex items-center gap-1"
        >
          <input
            class="radio radio-sm"
            type="radio"
            v-model="currentRangeCode"
            :value="`w${i - 1}`"
            @input="onRangeCodeSelected(`w${i - 1}`)"
          />
          <DateRangePanel
            :code="`w${i - 1}`"
            :time-zone="props.timeZone"
          ></DateRangePanel>
        </label>
      </div>
      <div v-if="quickRange.m" class="flex items-center flex-wrap gap-1">
        <div
          class="w-6 h-6 mr-2 text-sm border rounded flex items-center justify-center"
        >
          {{ la.t('.mark.m') }}
        </div>
        <label
          v-for="i in quickRange.m"
          :key="`quickRange_m${i - 1}`"
          class="mr-2 cursor-pointer flex items-center gap-1"
        >
          <input
            class="radio radio-sm"
            type="radio"
            v-model="currentRangeCode"
            :value="`m${i - 1}`"
            @input="onRangeCodeSelected(`m${i - 1}`)"
          />
          <DateRangePanel
            :code="`m${i - 1}`"
            :time-zone="props.timeZone"
          ></DateRangePanel>
        </label>
      </div>
      <div v-if="quickRange.q" class="flex items-center flex-wrap gap-1">
        <div
          class="w-6 h-6 mr-2 text-sm border rounded flex items-center justify-center"
        >
          {{ la.t('.mark.q') }}
        </div>
        <label
          v-for="i in quickRange.q"
          :key="`quickRange_q${i - 1}`"
          class="mr-2 cursor-pointer flex items-center gap-1"
        >
          <input
            class="radio radio-sm"
            type="radio"
            v-model="currentRangeCode"
            :value="`q${i - 1}`"
            @input="onRangeCodeSelected(`q${i - 1}`)"
          />
          <DateRangePanel
            :code="`q${i - 1}`"
            :time-zone="props.timeZone"
          ></DateRangePanel>
        </label>
      </div>
      <div v-if="quickRange.y" class="flex items-center flex-wrap gap-1">
        <div
          class="w-6 h-6 mr-2 text-sm border rounded flex items-center justify-center"
        >
          {{ la.t('.mark.y') }}
        </div>
        <label
          v-for="i in quickRange.y"
          :key="`quickRange_y${i - 1}`"
          class="mr-2 cursor-pointer flex items-center gap-1"
        >
          <input
            class="radio radio-sm"
            type="radio"
            v-model="currentRangeCode"
            :value="`y${i - 1}`"
            @input="onRangeCodeSelected(`y${i - 1}`)"
          />
          <DateRangePanel
            :code="`y${i - 1}`"
            :time-zone="props.timeZone"
          ></DateRangePanel>
        </label>
      </div>
      <div class="flex flex-col gap-3">
        <label class="flex items-center gap-1">
          <input
            class="radio radio-sm"
            type="radio"
            v-model="currentRangeCode"
            value="customized"
            @input="onRangeCodeSelected('customized')"
          />
          {{ la.t('.customizedRange') }}
        </label>
        <div
          v-if="currentRangeCode === 'customized'"
          class="m-0.5 flex flex-col gap-3"
        >
          <button
            class="input input-bordered flex items-center gap-2 focus:outline-primary focus:outline-2"
            @click="openPickerDialog('from')"
          >
            <IconPanel
              class="text-base-300"
              :icon-info="{
                icon: getIcon('mdi', 'ray-start'),
                size: 1.5
              }"
            ></IconPanel>
            <div
              v-if="customizedRange.from === undefined"
              class="text-base-content opacity-40"
            >
              {{ la.t('.fromDate') }}
            </div>
            <div v-else>
              {{
                formatDateWithTz(props.timeZone, customizedRange.from, {
                  dateOnly: true
                })
              }}
            </div>
          </button>
          <button
            class="input input-bordered flex items-center gap-2 focus:outline-primary focus:outline-2"
            @click="openPickerDialog('to')"
          >
            <IconPanel
              class="text-base-300"
              :icon-info="{
                icon: getIcon('mdi', 'ray-end'),
                size: 1.5
              }"
            ></IconPanel>
            <div
              v-if="customizedRange.to === undefined"
              class="text-base-content opacity-40"
            >
              {{ la.t('.toDate') }}
            </div>
            <div v-else>
              {{
                formatDateWithTz(props.timeZone, customizedRange.to, {
                  dateOnly: true
                })
              }}
            </div>
          </button>
        </div>
      </div>
    </div>
    <DateTimeSelectorDialog
      ref="dtSelectorDialog"
      :dt="inputDate"
      :time-zone="props.timeZone"
      :min-dt="selectorRange.from"
      :max-dt="selectorRange.to"
      :enable-fast-selection="true"
      :enable-clear-btn="true"
      :dialog-title="pickerDiglogTitle"
      :title-icon-info="pickerDiglogTitleIconInfo"
      @select="onDateTimePicked"
    ></DateTimeSelectorDialog>
  </BlockPanel>
</template>
