export type DateRangeDirection = 'from' | 'to';

export type DateRange = {
  from?: Date;
  to?: Date;
};

const SINCE_RANGE = ['1d', '1w', '1m', '1q', '6m', '1y'] as const;
export type SinceDateRange = (typeof SINCE_RANGE)[number];

export type QuickDateRange = {
  d?: number;
  w?: number;
  m?: number;
  q?: number;
  y?: number;
};

export const TIME_MARK = ['d', 'w', 'm', 'q', 'y'] as const;
export type TimeMark = (typeof TIME_MARK)[number];

export type RangeParsedResult = {
  kind: 'since' | 'quick';
  mark: TimeMark;
  count: number;
};
