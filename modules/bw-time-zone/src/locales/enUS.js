import data from './data/en-US.json' assert { type: 'json' };
import components from './components/en-US.json' assert { type: 'json' };

export default {
  data,
  components
};
