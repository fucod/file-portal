import data from './data/ja-JP.json' assert { type: 'json' };
import components from './components/ja-JP.json' assert { type: 'json' };

export default {
  data,
  components
};
