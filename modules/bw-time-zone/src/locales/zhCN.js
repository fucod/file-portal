import data from './data/zh-CN.json' assert { type: 'json' };
import components from './components/zh-CN.json' assert { type: 'json' };

export default {
  data,
  components
};
