import data from './data/zh-TW.json' assert { type: 'json' };
import components from './components/zh-TW.json' assert { type: 'json' };

export default {
  data,
  components
};
