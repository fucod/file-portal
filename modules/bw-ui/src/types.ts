import type { IconInfo } from '@base-workspace/bw-icon';

export type LabelInfo = {
  iconInfo: IconInfo;
  text?: string;
  colorName?: string;
};

export type IndexLink = {
  iconInfo: IconInfo;
  label: string;
  url: string;
};
