#!/bin/bash

SCRIPTS_DIR_PATH=$(dirname $(readlink -f "$0"))

source $SCRIPTS_DIR_PATH/envs.sh

show_help_and_exit() {
  echo "Usage: $0 -t target [-v verbose]"
  echo ""
  echo "  This script generates icon-bundle.json content according to 'getIcon' used in codes from apps/ and modules/."
  echo ""
  echo "  Options:"
  echo "    -v, --verbose <(none)|info|debug|trace>: The verbose level."
  echo "    -vv: Use the 'debug' verbose level."
  echo ""
  exit 0
}

BB_VERBOSE="none"

while [[ "$1" != "" ]]; do
  case $1 in
  -v | --verbose)
    shift
    BB_VERBOSE=$1
    ;;
  -vv)
    BB_VERBOSE="debug"
    ;;
  -h | --help)
    show_help_and_exit $0
    ;;
  *)
    echo -e "${ERROR_COLOR}ERROR: Invalid option: $1${NO_COLOR}"
    show_help_and_exit $0
    ;;
  esac
  shift
done

set -e

if [ -z "$BB_VERBOSE" ]; then
  BB_VERBOSE="info"
fi

if [ "$BB_VERBOSE" != "none" ] && [ "$BB_VERBOSE" != "info" ] && [ "$BB_VERBOSE" != "debug" ] && [ "$BB_VERBOSE" != "trace" ]; then
  echo -e "${ERROR_COLOR}ERROR -v: Invalid option: ${BB_VERBOSE}${NO_COLOR}"
  exit 1
fi

if [ "$BB_VERBOSE" == "trace" ]; then
  BB_VERBOSE="debug"
  set -x
fi

declare -A icon_set_path_map
icon_set_path_map["gi"]="game-icons"
icon_set_path_map["mdi"]="mdi"
icon_set_path_map["ph"]="ph"

for icon_set in "${!icon_set_path_map[@]}"; do
  icon_json_path="$REPO_ROOT_PATH/node_modules/@iconify-json/${icon_set_path_map[$icon_set]}/icons.json"
  if [ ! -f "$icon_json_path" ]; then
    echo -e "${ERROR_COLOR}ERROR: '$icon_json_path' not found.${NO_COLOR}"
    exit 1
  fi
done

declare -A icon_map

search_paths=("$REPO_ROOT_PATH/apps" "$REPO_ROOT_PATH/modules")
for file in "${search_paths[@]}"; do
  if grep -r -q "getIcon('" "$file"; then
    while IFS= read -r line; do
      IFS=',' read -r aaa bbb <<<"$(echo "$line" | awk -F"[()]" '{print $2}' | sed "s/'//g")"
      aaa=$(echo "$aaa" | xargs)
      bbb=$(echo "$bbb" | xargs)
      if [[ -z "${icon_map[$aaa]}" ]]; then
        icon_map[$aaa]="$bbb"
      else
        if [[ ! "${icon_map[$aaa]}" =~ (^|[[:space:]])"$bbb"($|[[:space:]]) ]]; then
          icon_map[$aaa]+=" $bbb"
        fi
      fi
    done < <(grep -r "getIcon('" "$file")
  fi
done

if [ "$BB_VERBOSE" == "debug" ]; then
  echo -e "\n${DEBUG_COLOR}DEBUG: Icon Map:${NO_COLOR}\n----"
  for key in "${!icon_map[@]}"; do
    echo -e "\n${DEBUG_COLOR}$key: [${icon_map[$key]}]${NO_COLOR}"
  done
  echo -e "----"
fi

ICON_BUNDLE_JSON_CONTENT="{"
sorted_icon_sets=($(printf "%s\n" "${!icon_map[@]}" | sort))
for icon_set in "${sorted_icon_sets[@]}"; do
  icon_json_path="$REPO_ROOT_PATH/node_modules/@iconify-json/${icon_set_path_map[$icon_set]}/icons.json"
  IFS=' ' read -r -a values <<<"${icon_map[$icon_set]}"
  sorted_values=($(printf "%s\n" "${values[@]}" | sort))
  for icon_name in "${sorted_values[@]}"; do
    icon_data=$(jq -r ".icons[\"${icon_name}\"]" $icon_json_path)
    ICON_BUNDLE_JSON_CONTENT="${ICON_BUNDLE_JSON_CONTENT}\n  \"${icon_set}_${icon_name}\": ${icon_data},"
  done
done
ICON_BUNDLE_JSON_CONTENT="${ICON_BUNDLE_JSON_CONTENT}\n}"
ICON_BUNDLE_JSON_CONTENT=$(echo -e $ICON_BUNDLE_JSON_CONTENT | sed ':a;N;$!ba;s/},\n}/}\n}/g' | jq '.')

if [ "$BB_VERBOSE" == "info" ] || [ "$BB_VERBOSE" == "debug" ]; then
  echo -e "${INFO_COLOR}INFO: '$0' done.${NO_COLOR}"
  echo -e "----"
fi

echo -e $ICON_BUNDLE_JSON_CONTENT
