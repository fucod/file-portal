#!/bin/bash

npm i
npm run prepare-build
npx nx build $1 -- --outDir ../../../public
