#!/bin/bash

SCRIPTS_DIR_PATH=$(dirname $(readlink -f "$0"))

source $SCRIPTS_DIR_PATH/envs.sh

show_help_and_exit() {
  echo "Usage: $0 -t target [-v verbose]"
  echo ""
  echo "  This script modify the current repo to be ready for build app."
  echo ""
  echo "  Options:"
  echo "    -r, --rollback: Rollback the changes by running this script."
  echo "    -v, --verbose <(none)|info|debug|trace>: The verbose level."
  echo "    -vv: Use the 'debug' verbose level."
  echo ""
  show_related_paths
  exit 0
}

BB_VERBOSE="none"
BB_ACTION="prepare"

while [[ "$1" != "" ]]; do
  case $1 in
  -r | --rollback)
    shift
    BB_ACTION="rollback"
    ;;
  -v | --verbose)
    shift
    BB_VERBOSE=$1
    ;;
  -vv)
    BB_VERBOSE="debug"
    ;;
  -h | --help)
    show_help_and_exit $0
    ;;
  *)
    echo -e "${ERROR_COLOR}ERROR: Invalid option: $1${NO_COLOR}"
    show_help_and_exit $0
    ;;
  esac
  shift
done

set -e

if [ -z "$BB_VERBOSE" ]; then
  BB_VERBOSE="info"
fi

if [ "$BB_VERBOSE" != "none" ] && [ "$BB_VERBOSE" != "info" ] && [ "$BB_VERBOSE" != "debug" ] && [ "$BB_VERBOSE" != "trace" ]; then
  echo -e "${ERROR_COLOR}ERROR -v: Invalid option: ${BB_VERBOSE}${NO_COLOR}"
  exit 1
fi

if [ "$BB_VERBOSE" == "trace" ]; then
  BB_VERBOSE="debug"
  set -x
fi

cd $REPO_ROOT_PATH

BUILD_LICENSES_PATH=$(find $REPO_ROOT_PATH/apps -type f -path "*/build/licenses.json")
if [ "$BB_ACTION" == "prepare" ]; then
  npm run gen-licenses --silent >$BUILD_LICENSES_PATH
  npm run gen-icon-bundle --silent >$REPO_ROOT_PATH/modules/bw-icon/src/icon-bundle.json
  mv $REPO_ROOT_PATH/modules/bw-icon/src/fetch-icon.ts $REPO_ROOT_PATH/modules/bw-icon/src/fetch-icon-package.ts
  cp $REPO_ROOT_PATH/modules/bw-icon/src/fetch-icon-bundle.ts $REPO_ROOT_PATH/modules/bw-icon/src/fetch-icon.ts
fi

if [ "$BB_ACTION" == "rollback" ]; then
  echo "[]" >$BUILD_LICENSES_PATH
  echo "{}" >$REPO_ROOT_PATH/modules/bw-icon/src/icon-bundle.json
  mv $REPO_ROOT_PATH/modules/bw-icon/src/fetch-icon.ts $REPO_ROOT_PATH/modules/bw-icon/src/fetch-icon-bundle.ts
  mv $REPO_ROOT_PATH/modules/bw-icon/src/fetch-icon-package.ts $REPO_ROOT_PATH/modules/bw-icon/src/fetch-icon.ts
fi

if [ "$BB_VERBOSE" == "info" ] || [ "$BB_VERBOSE" == "debug" ]; then
  echo -e "${INFO_COLOR}INFO: '$0' (${BB_ACTION}) done.${NO_COLOR}"
fi
