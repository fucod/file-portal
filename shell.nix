{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  buildInputs = with pkgs; [ nodejs_22 jq ];
  shellHook = ''
    NPM_GLOBAL_DIR="./.npm-global"
    export PATH="$NPM_GLOBAL_DIR/bin:$PATH"
    if [ -d "$NPM_GLOBAL_DIR" ]; then
      echo "[INFO]: The Dev environment is ready."
    else
      echo "[INFO]: Setup the Dev environment..."
      mkdir -p "$NPM_GLOBAL_DIR"
      npm config set prefix "$NPM_GLOBAL_DIR"
      npm install -g nx prettier-eslint-cli
      echo "[INFO]: Setup complete. The Dev environment is ready."
    fi
  '';
}
